package com.bit.bitcare.mcflp2nd.ui.activity.media

import android.app.Activity
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Environment
import android.view.View
import android.widget.MediaController
import android.widget.Toast
import android.widget.VideoView

import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.ui.activity.BaseActivity
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil
import kotlinx.android.synthetic.main.activity_play.*

import java.io.File

class PlayActivity : BaseActivity() {
    private var videoView: VideoView? = null
    private var mediaController: MediaController? = null

//    private val outputMediaFile: File
//        get() {
//            val recordPath = Environment.getExternalStorageDirectory().absolutePath
//            return File(recordPath + "/NIBP/" + File.separator + PreferenceUtil(this).getStringExtra("last_mp4") + ".mp4")
//        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play)

        mediaController = MediaController(this)
        video_view!!.setMediaController(mediaController)

        video_view!!.setOnPreparedListener {
            video_view!!.start()
            mediaController!!.show()
        }

        val filePath = intent.getStringExtra("willSendFile")

        if(filePath != null){
            video_view!!.setVideoPath(filePath)
        }else{
            Toast.makeText(this@PlayActivity, "영상을 재생할 수 없습니다.", Toast.LENGTH_LONG).show()
        }
    }
}
