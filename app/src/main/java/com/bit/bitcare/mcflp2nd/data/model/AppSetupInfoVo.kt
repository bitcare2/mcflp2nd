package com.bit.bitcare.mcflp2nd.data.model

data class AppSetupInfoVo(
    var appClsCode: String?,
    var newYn: String?,
    var appOperSysMfg: String?,
    var appOpfg: String?,
    var appOpMsg: String?,
    var appMummVer: String?,
    var enterDate: String?
)