package com.bit.bitcare.mcflp2nd.ui.activity.certification;

/**
 * Created by 정찬 on 2017-03-17.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bit.bitcare.mcflp2nd.R;
import com.yettiesoft.koreamint.certificate.ExtractCertificate;
import com.yettiesoft.koreamint.certificate.exception.KMException;
import com.yettiesoft.koreamint.certificate.pkcs.CertInfo;

public class KoreaMintMain extends AppCompatActivity {
    //////////////////////////////////////////////////////////////
    // 잉카 키패드
    NOSKeypad keypad = null;
    String js = null;

    EditText pwdText = null;
    String url = null;
    CertItemRecyclerViewAdapter adapter;
    AppCompatActivity mainContext;

    //////////////////////////////////////////////////////////////
    // 인증서 리스트 등록 및 인증서 선택, 인증서 내보내기 생성 Class
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cert_list);

        Intent intent = getIntent();
        this.url = intent.getExtras().getString("url");

        View recyclerView = findViewById(R.id.item_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);

        pwdText = (EditText) findViewById(R.id.passwordText);

        //////////////////////////////////////////////////////////////
        // 잉카 키패드 생성 및 키패드 적용 EditText 등록
        keypad = new NOSKeypad(this);
        keypad.init(pwdText);

        mainContext = this;
        Button appBtnOK = (Button) findViewById(R.id.btn_ok);
        appBtnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(adapter.getItemCount()!=0){
                    try {
                        //////////////////////////////////////////////////////////////
                        // 선택된 인증서 내보내기
                        js = exportCertificate(adapter.getSelectedSubjectName());

                        startMainActivity(js);
                    } catch (KMException e) {
                        Toast.makeText(mainContext, R.string.invalid_pwd, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(mainContext, R.string.invalid_cert, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        //////////////////////////////////////////////////////////////
        // 인증서 리스트 함수 호출
        adapter = new CertItemRecyclerViewAdapter(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //////////////////////////////////////////////////////////////
        // 잉카 키패드 핼퍼에 결과 정리 호출
        if (keypad.finalize(requestCode, resultCode, data)) {
            return;
        }
    }

    // 인증서 내보내기 함수
    public String exportCertificate(CertInfo certInfo) throws KMException {
        //////////////////////////////////////////////////////////////
        // keypad를 통해 비밀번호 가져오기
        String p12Pwd = keypad.getResult(pwdText);
        keypad.finish();
        String base64P12;

        base64P12 = ExtractCertificate.makePFXNoSloth(this.url, p12Pwd, certInfo);

        return base64P12;
    }

    public void startMainActivity(String js) {
        Intent intent = new Intent();
        intent.putExtra("js", js);
        setResult(0, intent);
        finish();
    }
}
