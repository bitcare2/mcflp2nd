package com.bit.bitcare.mcflp2nd

import android.app.Application
import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.res.Resources
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import android.util.DisplayMetrics
import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.interceptors.HttpLoggingInterceptor
import com.bit.bitcare.mcflp2nd.util.Dlog
import com.crashlytics.android.Crashlytics
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.jacksonandroidnetworking.JacksonParserFactory
import io.fabric.sdk.android.Fabric
import io.fabric.sdk.android.Fabric.isDebuggable


/**
 * Created by acid on 2018-08-02.
 */

class BaseApplication : MultiDexApplication() {
    init {
        instance = this
    }

    companion object {
        var instance: BaseApplication? = null
        var sw:Float = 0F

        val globalApplicationContext: BaseApplication?
            get() {
                if (instance == null)
                    throw IllegalStateException("this application does not inherit BaseApplication") as Throwable
                return instance
            }
        var DEBUG: Boolean = false

        fun context(): Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()

        MultiDex.install(this)
        BaseApplication.DEBUG = isDebuggable(this)
        Fabric.with(this, Crashlytics())

        val scaleFactor = Resources.getSystem().displayMetrics.density
        val widthDp = Resources.getSystem().displayMetrics.widthPixels / scaleFactor
        val heightDp = Resources.getSystem().displayMetrics.heightPixels / scaleFactor

        val smallestWidth = Math.min(widthDp, heightDp)
        sw = smallestWidth
        if (smallestWidth > 720) {
            Dlog.e("Device is a 10\" inch smallestWidth $smallestWidth")
        }
        else if (smallestWidth > 600) {
            Dlog.e("Device is a 7\" inch smallestWidth $smallestWidth")
        } else {
            Dlog.e("Device is a other smallestWidth $smallestWidth")
        }

        Dlog.e("density : $scaleFactor")
        Dlog.e("densityDpi : "+Resources.getSystem().displayMetrics.densityDpi)
        Dlog.e("heightPixels : "+Resources.getSystem().displayMetrics.heightPixels)
        Dlog.e("widthPixels : "+Resources.getSystem().displayMetrics.widthPixels)
        Dlog.e("scaledDensity : "+Resources.getSystem().displayMetrics.scaledDensity)

        val widthDpi = Resources.getSystem().displayMetrics.xdpi
        val heightDpi = Resources.getSystem().displayMetrics.ydpi

        val widthInches = Resources.getSystem().displayMetrics.heightPixels / widthDpi
        val heightInches = Resources.getSystem().displayMetrics.heightPixels / heightDpi

        val diagonalInches = Math.sqrt((widthInches * widthInches + heightInches * heightInches).toDouble())

        if (diagonalInches >= 10) {
            Dlog.e("Device is a 10\" inch Real $diagonalInches inch")
        }
        else if (diagonalInches >= 7) {
            Dlog.e("Device is a 7\" inch Real $diagonalInches inch")
        }
        else {
            Dlog.e("Device is a other $diagonalInches inch")
        }

//        AndroidNetworking.enableLogging()

        //Firebase initialization for custom setting
        val builder: FirebaseOptions.Builder = FirebaseOptions.Builder()
                .setApplicationId("1:338993658065:android:42de6bdebfdcef26")
                .setApiKey("AIzaSyD7VbtvFamT685L5vzYx_spsdx_B86-jW4")
//                .setDatabaseUrl("https://exampleformcflp2nd.firebaseio.com")
//                .setStorageBucket("exampleformcflp2nd.appspot.com")
//
        FirebaseApp.initializeApp(this, builder.build())
//
////        // Adding an Network Interceptor for Debugging purpose :
////        val okHttpClient = OkHttpClient().newBuilder()
////                .addNetworkInterceptor(StethoInterceptor())
////                .build()
////        AndroidNetworking.initialize(applicationContext, okHttpClient)
//
        AndroidNetworking.initialize(applicationContext)
        AndroidNetworking.setParserFactory(JacksonParserFactory())
//        AndroidNetworking.enableLogging()
//        AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BODY)
    }

    /**
     * 현재 디버그모드 여부를 리턴
     */
    private fun isDebuggable(context:Context):Boolean {
        var debuggable = false
        val pm = context.getPackageManager()
        try {
            val appinfo = pm.getApplicationInfo(context.getPackageName(), 0)
            debuggable = (0 != (appinfo.flags and ApplicationInfo.FLAG_DEBUGGABLE))
        }
        catch (e: PackageManager.NameNotFoundException) {
            /* debuggable variable will remain false */
        }
        return debuggable
    }

}

//////////////////////////////////////////////////////
////////    Test기기 displayMetrics 정보      ////////
//////////////////////////////////////////////////////
//    SM-T580 / 삼성 A6 10.1인치
//    [BaseApplication.kt::onCreate]density : 1.5
//    [BaseApplication.kt::onCreate]densityDpi : 240
//    [BaseApplication.kt::onCreate]heightPixels : 1920
//    [BaseApplication.kt::onCreate]widthPixels : 1200
//    [BaseApplication.kt::onCreate]scaledDensity : 1.5

