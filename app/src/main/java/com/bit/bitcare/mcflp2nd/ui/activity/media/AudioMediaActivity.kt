package com.bit.bitcare.mcflp2nd.ui.activity.media

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDialog
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.StringRequestListener
import com.androidnetworking.interfaces.UploadProgressListener
import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.ui.activity.BaseActivity
import com.bit.bitcare.mcflp2nd.ui.dialog.FoldingCubeDialog
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.Dlog
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil
import kotlinx.android.synthetic.main.activity_audio_media.*
import kotlinx.android.synthetic.main.activity_camera_media.*
import java.io.File

import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class AudioMediaActivity : BaseActivity() {
    private var statusTextView: TextView? = null
//    private var playButton: Button? = null
//    private var recordButton: Button? = null

    private var currentType: Int = -1 // 0:녹음, 1:재생
    private var fileName: String? = null
    private var willSendFile:File? = null
    private var filegrpno:String? = null
    private var mediaRecorder: MediaRecorder? = null
    private var mediaPlayer: MediaPlayer? = null
    private var foldingCubeDialog: FoldingCubeDialog? = null

    val outputMediaFile: File?
        get() {
//            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED, ignoreCase = true)) {
                val filedir = File(Environment.getExternalStorageDirectory(), "NIBP")
                if (!filedir.exists()) {
                    filedir.mkdir()
                }
                val currenttime = SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA).format(Date())
                return File(filedir.absolutePath + File.separator + "Audio_"+ currenttime + ".m4a")
//            }
            return null
        }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var permissionToRecordAccepted = false
        when (requestCode) {
            Define.REQUEST_RECORD_VOICE_PERMISSION -> permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED
        }
        if (permissionToRecordAccepted == false) {
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_audio_media)
        setFinishOnTouchOutside(false)

        foldingCubeDialog = FoldingCubeDialog(this)

        filegrpno = intent?.extras?.getString("filegrpno")

        for (permission in Define.needVoiceRecordPermissions) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, Define.needVideoRecordPermissions, Define.REQUEST_RECORD_VIDEO_PERMISSION)
                break
            }
        }

        fileName = outputMediaFile!!.absolutePath
        PreferenceUtil(this).putStringExtra("last_audio_file", fileName)

        willSendFile = File(fileName)
        statusTextView = findViewById<View>(R.id.status_textview) as TextView

        record_button_voice!!.setOnClickListener {
            if (mediaRecorder == null) {
                startRecording()
            } else {
                stopRecording()
            }
        }
        record_button_voice_icon!!.setOnClickListener {
            if (mediaRecorder == null) {
                startRecording()
            } else {
                stopRecording()
            }
        }
        play_button_voice!!.setOnClickListener {
            if (mediaPlayer == null) {
                startPlaying()
            } else {
                stopPlaying()
            }
        }
        play_button_voice_icon!!.setOnClickListener {
            if (mediaPlayer == null) {
                startPlaying()
            } else {
                stopPlaying()
            }
        }
        upload_button_voice!!.setOnClickListener {
            if(willSendFile!!.exists()){
                sendFile(willSendFile!!)
            } else {
                Toast.makeText(this,"녹음된 파일이 없습니다.",Toast.LENGTH_LONG).show()
            }
        }
        upload_button_voice_icon!!.setOnClickListener{
            if(willSendFile!!.exists()){
                sendFile(willSendFile!!)
            } else {
                Toast.makeText(this,"녹음된 파일이 없습니다.",Toast.LENGTH_LONG).show()
            }
        }
        close_button_voice!!.setOnClickListener{
            exit()
        }
    }

    private fun startRecording() {
        record_button_voice_icon.setBackgroundResource(R.drawable.ic_stop_gray_48dp)
        record_button_voice!!.text = "중지"

        play_button_voice.isEnabled = false
        play_button_voice.isFocusable = false
        play_button_voice.isClickable = false
        play_button_voice_icon.isEnabled = false
        play_button_voice_icon.isFocusable = false
        play_button_voice_icon.isClickable = false
        upload_button_voice.isEnabled = false
        upload_button_voice.isFocusable = false
        upload_button_voice.isClickable = false
        upload_button_voice_icon.isEnabled = false
        upload_button_voice_icon.isFocusable = false
        upload_button_voice_icon.isClickable = false

        mediaRecorder = MediaRecorder()
        mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
        mediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
        mediaRecorder!!.setOutputFile(fileName)
        mediaRecorder!!.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_WB)
        mediaRecorder!!.setAudioSamplingRate(44100)
        mediaRecorder!!.setAudioEncodingBitRate(48000)

        try {
            mediaRecorder!!.prepare()
            startTime()
            mediaRecorder!!.start()
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(this, "녹음에 실패하였습니다.", Toast.LENGTH_LONG).show()
            mediaRecorder?.stop()
            mediaRecorder?.reset()
            mediaRecorder?.release()
            mediaRecorder = null
            allButtonEnable()
        }
    }

    private fun stopRecording() {
        record_button_voice_icon.setBackgroundResource(R.drawable.ic_mic_gray_48dp)
        record_button_voice!!.text = "녹음"

        play_button_voice.isEnabled = true
        play_button_voice.isFocusable = true
        play_button_voice.isClickable = true
        play_button_voice_icon.isEnabled = true
        play_button_voice_icon.isFocusable = true
        play_button_voice_icon.isClickable = true
        upload_button_voice.isEnabled = true
        upload_button_voice.isFocusable = true
        upload_button_voice.isClickable = true
        upload_button_voice_icon.isEnabled = true
        upload_button_voice_icon.isFocusable = true
        upload_button_voice_icon.isClickable = true

        stopTime()

        if (mediaRecorder != null) {
            mediaRecorder!!.stop()
            mediaRecorder!!.reset()
            mediaRecorder!!.release()
            mediaRecorder = null
        }
    }

    private var currentTime:Int = 0
    private var timer: Timer? = null
    private var timerTask: TimerTask? = null
    private var timerBuffer: String = ""

    private fun startTime(){
        currentTime = System.currentTimeMillis().toInt() / 1000
        timerTask = object : TimerTask() {
            override fun run() {
                updateNumber()
            }
        }

        timer = Timer()
        timer!!.schedule(timerTask, 0, 1000)
    }

    private  fun stopTime(){
        timer!!.cancel()
        timer = null
    }

    private fun updateNumber() {
//        Dlog.e("currentTime : " + currentTime.toString() + "")

//        if (leftTime == 0) {
//            timer!!.cancel()
//            timer = null
//        } else {
            runOnUiThread {
                secToHHMMSS(System.currentTimeMillis().toInt() / 1000 - currentTime)
//                currentTime++
//            }
        }
    }

    fun secToHHMMSS(secs: Int) {
//        val hour: Int
//        val min: Int
//        val sec: Int
//
//        sec = secs % 60
//        min = secs / 60 % 60
//        hour = secs / 3600

        runOnUiThread {
            statusTextView!!.text = String.format("%02d:%02d:%02d", secs / 3600, secs / 60 % 60, secs % 60)
//                currentTime++
//            }
        }
    }

    private fun startPlaying() {
//        statusTextView!!.text = "재생중"
        play_button_voice!!.text = "재생중지"
        play_button_voice_icon.setBackgroundResource(R.drawable.ic_stop_gray_48dp)

        record_button_voice.isEnabled = false
        record_button_voice.isFocusable = false
        record_button_voice.isClickable = false
        record_button_voice_icon.isEnabled = false
        record_button_voice_icon.isFocusable = false
        record_button_voice_icon.isClickable = false
        upload_button_voice.isEnabled = false
        upload_button_voice.isFocusable = false
        upload_button_voice.isClickable = false
        upload_button_voice_icon.isEnabled = false
        upload_button_voice_icon.isFocusable = false
        upload_button_voice_icon.isClickable = false

        mediaPlayer = MediaPlayer()
        mediaPlayer!!.setOnCompletionListener { stopPlaying() }

        try {
            mediaPlayer!!.setDataSource(fileName)
            mediaPlayer!!.prepare()
            mediaPlayer!!.start()
            startTime()
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(this, "재생에 실패하였습니다.", Toast.LENGTH_LONG).show()
            play_button_voice!!.text = "재생"
            play_button_voice_icon.setBackgroundResource(R.drawable.ic_play_gray_48dp)
            mediaPlayer = null
            allButtonEnable()
        }

    }

    private fun stopPlaying() {
        play_button_voice!!.text = "재생"
        play_button_voice_icon.setBackgroundResource(R.drawable.ic_play_gray_48dp)

        record_button_voice.isEnabled = true
        record_button_voice.isFocusable = true
        record_button_voice.isClickable = true
        record_button_voice_icon.isEnabled = true
        record_button_voice_icon.isFocusable = true
        record_button_voice_icon.isClickable = true
        upload_button_voice.isEnabled = true
        upload_button_voice.isFocusable = true
        upload_button_voice.isClickable = true
        upload_button_voice_icon.isEnabled = true
        upload_button_voice_icon.isFocusable = true
        upload_button_voice_icon.isClickable = true

        stopTime()

        if (mediaPlayer != null) {
            mediaPlayer!!.release()
            mediaPlayer = null
        }
    }

    private fun sendFile(file: File){
        Dlog.e("sendFile")
        Dlog.e("file : "+file.absolutePath)

        val uploadProgressListener = UploadProgressListener { bytesUploaded, totalBytes ->
            Dlog.e("send file onProgress : "+bytesUploaded.toString()+"%")
            var per:Long = bytesUploaded*100/totalBytes
            Dlog.e("send file onProgress : " + per.toString()+"%")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                progress_bar_record_voice.setProgress(per.toInt(), true)
            }else{
                foldingCubeDialog?.show()
            }
        }

        val stringRequestListener = object: StringRequestListener {
            override fun onResponse(response:String) {
                Dlog.e("send file onResponse : $response")

                foldingCubeDialog?.dismiss()

                //파일삭제
                if(outputMediaFile!!.exists()){
                    outputMediaFile!!.delete()
                }

                //이전 페이지로 이동
                var resultIntent = Intent()
                resultIntent.putExtra("result", response)
                setResult(Activity.RESULT_OK, resultIntent)
                finish()

            }
            override fun onError(error: ANError) {
                foldingCubeDialog?.dismiss()

                if (error.errorCode !== 0) {
                    // received error from server
                    // error.getErrorCode() - the error code from server
                    // error.getErrorBody() - the error body from server
                    // error.getErrorDetail() - just an error detail
                    Log.d("send file onError", "onError errorCode : " + error.errorCode)
                    Log.d("send file onError", "onError errorBody : " + error.errorBody)
                    Log.d("send file onError", "onError errorDetail : " + error.errorDetail)
                    // get parsed error object (If ApiError is your class)
//                    val apiError = error.getErrorAsObject(ApiError::class.java)
                } else {
                    // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                    Log.d("send file onError", "onError errorDetail : " + error.errorDetail)
                }
            }
        }

        AndroidNetworking.upload(Define.mobileServerIP+"/temp/file/upload")
                .addMultipartFile("file1",file)
                .addMultipartParameter("filegrpno",filegrpno!!.toString())
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(uploadProgressListener)
                .getAsString(stringRequestListener)
    }

    private fun allButtonEnable(){
        record_button_voice.isEnabled = true
        record_button_voice.isFocusable = true
        record_button_voice.isClickable = true
        record_button_voice_icon.isEnabled = true
        record_button_voice_icon.isFocusable = true
        record_button_voice_icon.isClickable = true
        play_button_voice.isEnabled = true
        play_button_voice.isFocusable = true
        play_button_voice.isClickable = true
        play_button_voice_icon.isEnabled = true
        play_button_voice_icon.isFocusable = true
        play_button_voice_icon.isClickable = true
        upload_button_voice.isEnabled = true
        upload_button_voice.isFocusable = true
        upload_button_voice.isClickable = true
        upload_button_voice_icon.isEnabled = true
        upload_button_voice_icon.isFocusable = true
        upload_button_voice_icon.isClickable = true
    }
    override fun onDestroy() {
        super.onDestroy()
        //파일삭제
        if(outputMediaFile!!.exists()){
            outputMediaFile!!.delete()
        }
    }

    override fun onBackPressed() {
        exit()
    }

    private fun exit(){
        if(willSendFile!!.exists()) {
            MaterialDialog(this)
                    .title(R.string.audio_exit)
                    .message(R.string.audio_exit_message)
                    .positiveButton (R.string.yes){
                        finish()
                    }
                    .negativeButton(R.string.no){

                    }
                    .show()
        }else{
            finish()
        }
    }
}
