package com.bit.bitcare.mcflp2nd.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.ui.activity.MainActivity
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.Dlog
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil
import kotlinx.android.synthetic.main.fragment_temporary_save.*

class TemporarySaveFragment : BaseFragment(){
    private val TAG = this::class.java.simpleName

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_temporary_save, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        settingWebView(webview_temporary_save)
        Dlog.e("Define.temporarySaveListUrl : "+Define.temporarySaveListUrl+ PreferenceUtil(activity).getStringExtra("token"))
        webview_temporary_save!!.loadUrl(Define.temporarySaveListUrl+ PreferenceUtil(activity).getStringExtra("token"))
    }

    override fun getCurrentViewName():String{
        return this::class.java.simpleName
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == AppCompatActivity.RESULT_OK) {
            Dlog.e("resultCode = $resultCode")
            Dlog.e("requestCode = $requestCode")
            Dlog.e("data = ${data.toString()}")

            when (requestCode) {
                Define.REQUEST_REG_FORM -> {
                    Dlog.e("data = " + data!!.getStringExtra("type"))
                    Dlog.e("data = " + data.getStringExtra("idtype"))
                    Dlog.e("data = " + data.getStringExtra("datakey"))
                    Dlog.e("data = " + data.getStringExtra("usernm"))

//                    webView!!.loadUrl("javascript:" + data!!.getStringExtra("type") + ".regFormCallback('" + data.getStringExtra("idtype") + "','"
//                            + data.getStringExtra("datakey") + "','"
//                            + data.getStringExtra("usernm") + "')")

                    webView!!.reload()
                }
            }
        }
    }

}