package com.bit.bitcare.mcflp2nd.ui.activity

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.WindowManager
import android.webkit.WebView
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.StringRequestListener
import com.bit.bitcare.mcflp2nd.BaseApplication
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.Dlog
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil
import java.util.ArrayList
import org.json.JSONArray
import com.androidnetworking.interfaces.JSONArrayRequestListener
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bit.bitcare.mcflp2nd.data.model.ResponseVo
import com.bit.bitcare.mcflp2nd.ui.dialog.WaveDialog
import com.google.gson.Gson
import org.json.JSONObject


open class BaseActivity : AppCompatActivity() {
    var gson = Gson()
    var waveDialog : WaveDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        waveDialog = WaveDialog(this)

        try {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        } catch (e: Exception) {

        }

        //크롬 인스펙트 이용가능
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            WebView.setWebContentsDebuggingEnabled(true)
//        }

    }

    override fun onResume() {
        super.onResume()
        window.addFlags(WindowManager.LayoutParams.FLAG_SECURE)

        val serverIp = PreferenceUtil(this@BaseActivity).getStringExtra("serverIP")
        val mobileServerIp = PreferenceUtil(this@BaseActivity).getStringExtra("mobileServerIP")

        Define.serverIP = serverIp
        Define.userSearchUrl                = "$serverIp/mobile/reg/mng/orgWrtMng/index.do?tabFlag=A&token="
        Define.temporarySaveListUrl         = "$serverIp/mobile/reg/mng/orgWrtMng/index.do?tabFlag=B&token="
        Define.JobHistoryUrl                = "$serverIp/mobile/reg/mng/orgWrtMng/index.do?tabFlag=C&token="
        Define.certLoginUrl                 = "$serverIp/rm/mobile/mobilecert.do"
        Define.mobileServerIP               = mobileServerIp
        
    }

    override fun setRequestedOrientation(requestedOrientation: Int) {
        /*
         if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
             // no-op
         }else{
             super.setRequestedOrientation(requestedOrientation);
         }
         */
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.O) {
            super.setRequestedOrientation(requestedOrientation);
        }
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(0,0)
    }

    private val stringRequestListener = object: StringRequestListener {
        override fun onResponse(response:String) {
            Log.e("send file onResponse",response.toString())
            Toast.makeText(this@BaseActivity, "토큰 받아옴", Toast.LENGTH_LONG).show()
            PreferenceUtil(this@BaseActivity).putStringExtra("token",response.toString())
        }
        override fun onError(error: ANError) {
            val errorCode = arrayListOf(error.errorCode)
            if (errorCode[0] !== 0) {
                // received error from server
                // error.getErrorCode() - the error code from server
                // error.getErrorBody() - the error body from server
                // error.getErrorDetail() - just an error detail
                Log.d("send file onError", "onError errorCode : " + error.errorCode)
                Log.d("send file onError", "onError errorBody : " + error.errorBody)
                Log.d("send file onError", "onError errorDetail : " + error.errorDetail)
                // get parsed error object (If ApiError is your class)
//                    val apiError = error.getErrorAsObject(ApiError::class.java)
            } else {
                // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                Log.d("send file onError", "onError errorDetail : " + error.errorDetail)
            }
        }
    }

    //권한 체크
    fun checkPermissions(permissions: Array<String>, permissionResultCode: Int): Boolean {
        Dlog.e("checkPermissions")
        var result: Int
        val permissionList = ArrayList<String>()
        for (pm in permissions) {
            result = ContextCompat.checkSelfPermission(this, pm)
            if (result != PackageManager.PERMISSION_GRANTED) { //사용자가 해당 권한을 가지고 있지 않을 경우 리스트에 해당 권한명 추가
                permissionList.add(pm)
                Dlog.e("permissionList : $pm")
            }
        }
        if (!permissionList.isEmpty()) { //권한이 추가되었으면 해당 리스트가 empty가 아니므로 request 즉 권한을 요청합니다.
            Dlog.e("permissionList : ${permissionList}")
            requestPermissions(this, permissionList.toTypedArray(), permissionResultCode)
            return false
        }
        return true
    }

    //권한 요청
    fun requestPermissions(requireActivity: Activity, toTypedArray: Array<String>, permissionResultCode: Int) {
        Dlog.e("requestPermissions")
        when (permissionResultCode) {
            Define.REQUEST_RECORD_VOICE_PERMISSION -> {
                for (permission in Define.needVoiceRecordPermissions) {
                    if (ActivityCompat.checkSelfPermission(baseContext, permission) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(requireActivity, Define.needVoiceRecordPermissions, permissionResultCode)
                        break
                    }
                }
            }

            Define.REQUEST_RECORD_VIDEO_PERMISSION -> {
                for (permission in Define.needVideoRecordPermissions) {
                    if (ActivityCompat.checkSelfPermission(baseContext, permission) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(requireActivity, Define.needVideoRecordPermissions, permissionResultCode)
                        break
                    }
                }
            }

            Define.REQUEST_FILE_PERMISSION -> {
                for (permission in Define.needVideoRecordPermissions) {
                    if (ActivityCompat.checkSelfPermission(baseContext, permission) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(requireActivity, Define.needFilePermissions, permissionResultCode)
                        break
                    }
                }
            }
        }
    }



}
