package com.bit.bitcare.mcflp2nd.util

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.webkit.JsResult
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.webkit.WebView
import com.bit.bitcare.mcflp2nd.ui.fragment.UserSearchFragment

class BitWebChromeClient(private val context:Context): WebChromeClient() {
    var mUploadMessage:ValueCallback<Uri>? = null
    var mUploadMessageArr:ValueCallback<Array<Uri>>? = null

    override fun onJsAlert(view: WebView, url:String, message:String, result: JsResult):Boolean {
        return super.onJsAlert(view, url, message, result)
    }

    // For Android < 3.0
    fun openFileChooser(uploadMsg: ValueCallback<Uri>) {
        mUploadMessage = uploadMsg
        val i = Intent(Intent.ACTION_GET_CONTENT)
        i.addCategory(Intent.CATEGORY_OPENABLE)
        i.setType("*/*")
//        context.applicationContext.stult(Intent.createChooser(i, "파일을 선택해 주세요"), UserSearchFragment.REQUEST_SELECT_FILE_LEGACY)

    }
    // For Android 3.0+
    fun openFileChooser(uploadMsg: ValueCallback<Uri>, acceptType:String) {
        mUploadMessage = uploadMsg
        val i = Intent(Intent.ACTION_GET_CONTENT)
        i.addCategory(Intent.CATEGORY_OPENABLE)
        i.setType(acceptType)
//        startActivityForResult(Intent.createChooser(i, "파일을 선택해 주세요"), UserSearchFragment.REQUEST_SELECT_FILE_LEGACY)
    }
    // For Android 4.1+
    fun openFileChooser(uploadMsg: ValueCallback<Uri>, acceptType:String, capture:String) {
        mUploadMessage = uploadMsg
        val i = Intent(Intent.ACTION_GET_CONTENT)
        i.addCategory(Intent.CATEGORY_OPENABLE)
        i.setType(acceptType)
//        startActivityForResult(Intent.createChooser(i, "파일을 선택해 주세요"), UserSearchFragment.REQUEST_SELECT_FILE_LEGACY)
    }


    // For Android 5.0+
    @SuppressLint("NewApi")
    override fun onShowFileChooser(webView: WebView, filePathCallback: ValueCallback<Array<Uri>>, fileChooserParams: WebChromeClient.FileChooserParams):Boolean {
        if (mUploadMessageArr != null)
        {
            mUploadMessageArr!!.onReceiveValue(null)
            mUploadMessageArr = null
        }
        mUploadMessageArr = filePathCallback
        val intent = fileChooserParams.createIntent()
        try
        {
//            startActivityForResult(intent, UserSearchFragment.REQUEST_SELECT_FILE)
        }
        catch (e: ActivityNotFoundException) {
            mUploadMessageArr = null
//                    Toast.makeText(get, "에러 - 파일선택", Toast.LENGTH_LONG).show()
            Dlog.e("에러 - 파일선택")
            return false
        }
        return true
    }
}