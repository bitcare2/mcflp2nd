package com.bit.bitcare.mcflp2nd.ui.fragment

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.DownloadManager
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Environment.getDownloadCacheDirectory
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v4.widget.NestedScrollView
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.*
import android.webkit.*
import android.widget.Toast
import com.bit.bitcare.mcflp2nd.BaseApplication
import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.ui.activity.EFormViewerActivity
import com.bit.bitcare.mcflp2nd.ui.activity.LoginActivity
import com.bit.bitcare.mcflp2nd.ui.activity.MainActivity
import com.bit.bitcare.mcflp2nd.ui.activity.PopUpActivity
import com.bit.bitcare.mcflp2nd.ui.activity.media.AudioMediaActivity
import com.bit.bitcare.mcflp2nd.ui.activity.media.CameraMediaActivity
import com.bit.bitcare.mcflp2nd.ui.dialog.WaveDialog
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.Dlog
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil
import kotlinx.android.synthetic.main.activity_pop_up.*
import kotlinx.android.synthetic.main.fragment_user_search.*
import java.io.File
import java.lang.Double
import java.text.SimpleDateFormat
import java.util.*

open class BaseFragment : Fragment() {
    var webView: WebView? = null
    var swipeRefreshLayout: CustSwipeRefreshLayout? = null
    var nestedScrollView: NestedScrollView? = null
    var webPageLoadSuccessFlag = "N"
    var waveDialog : WaveDialog? = null

    lateinit var destinationDir: File
    lateinit var manager: DownloadManager
    open lateinit var uri:Uri

    private var fileName: String? = null

    private lateinit var downloadUrl: String
    private lateinit var downloadUserAgent: String
    private lateinit var downloadContentDisposition: String
    private lateinit var downloadMimeType: String

    inner class BitInterface {
        @JavascriptInterface
        fun getToken(): String { //토큰 요청
            var token = PreferenceUtil(activity).getStringExtra("token")
            Dlog.e("token : $token")
            return token
        }

        @JavascriptInterface
        fun openWebView(title: String, url: String, option: String) { //팝업 열기
            Dlog.e("openPopup / $title / $url / $option")
            val intent = Intent(requireContext(), PopUpActivity::class.java)
            var bundle: Bundle = Bundle().apply {
                putString("title", title)
                putString("url", url)
                putString("option", option)
            }
            intent.putExtras(bundle)
            activity!!.startActivityForResult(intent,Define.REQUEST_REG_FORM)
            activity!!.overridePendingTransition(0, 0)
        }

        @JavascriptInterface
        fun openReport(title: String, url: String, option: String, userInfo: String) { //서식 열기
            Dlog.e("opeonReport / $title / $url / $option / $userInfo")
            val intent = Intent(activity, EFormViewerActivity::class.java)
            var bundle: Bundle = Bundle().apply {
                putString("title", title)
                putString("url", url)
                putString("option", option)
                putString("userInfo", userInfo)
            }
            intent.putExtras(bundle)
            startActivity(intent)
            activity!!.overridePendingTransition(0,0)
        }

        @JavascriptInterface
        fun sendSuccFlag(flag:String){
            webPageLoadSuccessFlag = flag
        }

        @JavascriptInterface
        fun goToLogin(){
            Dlog.e("goToLogin")
            moveLogin()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        waveDialog = WaveDialog(context!!)
    }

    open fun getCurrentViewName(): String {
        return this::class.java.simpleName
    }


    val outputMediaFile: File?
        get() {
//            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED, ignoreCase = true)) {
            val filedir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),"")
            if (!filedir.exists()) {
                filedir.mkdir()
            }
            val currenttime = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            return File(filedir.absolutePath + File.separator +"등록자관리업무이력_"+ currenttime + ".xlsx")
//            }
            return null
        }

    open fun handleUri(uri: Uri): Boolean {
        Dlog.e("handleUri $uri")
        var shouldOverride = false

        fileName = outputMediaFile!!.absolutePath

        // We only want to handle requests for mp3 files, everything else the webview
        // can handle normally
        if (uri.toString().contains("excelDownMedTaskHisList")) {
            Dlog.e("excelDownMedTaskHisList")
            shouldOverride = true

            val request = DownloadManager.Request(uri)

            val destinationFile = File(fileName)
            request.setDestinationUri(Uri.fromFile(destinationFile))
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
//            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/")
            // Add it to the manager
            manager.enqueue(request)
        }
        return shouldOverride
    }

    private fun moveLogin(){
        var intent = Intent(activity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        startActivity(intent)
        activity!!.overridePendingTransition(0,0)
    }

    fun settingWebView(paramWebView: WebView) {
        webView = paramWebView
        val settings = webView!!.settings
        settings.javaScriptEnabled = true
        settings.domStorageEnabled = true
        settings.databaseEnabled = true
        settings.useWideViewPort =  true
        settings.loadWithOverviewMode = true
        settings.setSupportZoom(true)
        settings.builtInZoomControls = true
        settings.displayZoomControls = false
        settings.setSupportMultipleWindows(true)
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.NORMAL
        settings.allowFileAccess = true

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            settings.safeBrowsingEnabled = false
        }

        manager = activity!!.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        destinationDir = File(Environment.getExternalStorageDirectory(), "NIBP")
        if (!destinationDir.exists()) {
            destinationDir.mkdir() // Don't forget to make the directory if it's not there
        }

        webView!!.addJavascriptInterface(BitInterface(), "App")
        webView!!.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                Dlog.e("waveDialog?.show()")
                try{
                    waveDialog?.show()
                }catch (e:java.lang.Exception){
                    Dlog.e(e.toString())
                }

            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                Dlog.e("waveDialog?.dismiss()")
                try{
                    waveDialog?.dismiss()
                }catch (e:java.lang.Exception){
                    Dlog.e(e.toString())
                }

            }

            override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
                //                handler.proceed(); // SSL 에러가 발생해도 계속 진행!
                val builder = AlertDialog.Builder(BaseApplication.context())
//                builder.setMessage(R.string.notification_error_ssl_cert_invalid)
                builder.setPositiveButton("continue", DialogInterface.OnClickListener { dialog, which -> handler.proceed() })
                builder.setNegativeButton("cancel", DialogInterface.OnClickListener { dialog, which -> handler.cancel() })
                val dialog = builder.create()
                dialog.show()
            }

            @TargetApi(Build.VERSION_CODES.M)
            override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
                super.onReceivedError(view, request, error)
                waveDialog?.dismiss()

                when (error.errorCode) {
                    WebViewClient.ERROR_AUTHENTICATION                      // 서버에서 사용자 인증 실패
                        , WebViewClient.ERROR_BAD_URL                             // 잘못된 URL
                        , WebViewClient.ERROR_CONNECT                             // 서버로 연결 실패
                        , WebViewClient.ERROR_FAILED_SSL_HANDSHAKE                // SSL handshake 수행 실패
                        , WebViewClient.ERROR_FILE                                // 일반 파일 오류
                        , WebViewClient.ERROR_FILE_NOT_FOUND                      // 파일을 찾을 수 없습니다
                        , WebViewClient.ERROR_HOST_LOOKUP                         // 서버 또는 프록시 호스트 이름 조회 실패
                        , WebViewClient.ERROR_IO                                  // 서버에서 읽거나 서버로 쓰기 실패
                        , WebViewClient.ERROR_PROXY_AUTHENTICATION                // 프록시에서 사용자 인증 실패
                        , WebViewClient.ERROR_REDIRECT_LOOP                       // 너무 많은 리디렉션
                        , WebViewClient.ERROR_TIMEOUT                             // 연결 시간 초과
                        , WebViewClient.ERROR_TOO_MANY_REQUESTS                   // 페이지 로드중 너무 많은 요청 발생
                        , WebViewClient.ERROR_UNKNOWN                             // 일반 오류
                        , WebViewClient.ERROR_UNSUPPORTED_AUTH_SCHEME             // 지원되지 않는 인증 체계
                        , WebViewClient.ERROR_UNSUPPORTED_SCHEME -> {

//                        val builder = AlertDialog.Builder(this@PrintActivity)
//                        //      builder.setTitle(R.string.ALRET_DIALOG_TITLE);
//                        builder.setPositiveButton("확인", DialogInterface.OnClickListener { dialog, which -> finish() })
//                        builder.setMessage("에러가 발생했습니다.")
//                        builder.show()
                    }
                }// URI가 지원되지 않는 방식

            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                Dlog.e("shouldOverrideUrlLoading1")
                val uri = Uri.parse(url)
                this@BaseFragment.uri = uri
                Dlog.e("uri = $uri")

                var result = checkPermissions(Define.needFilePermissions, Define.REQUEST_FILE_PERMISSION)
                Dlog.e("checkPermissions result : $result")
                return if (result) {
                    handleUri(uri)
                }else{
                    false
                }
//                return false
//                view.loadUrl(url)
//                return true//응용프로그램이 직접 url를 처리함
            }

            @TargetApi(Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                Dlog.e("shouldOverrideUrlLoading2")
                val uri = request.url
                this@BaseFragment.uri = uri
                Dlog.e("uri = $uri")

                var result = checkPermissions(Define.needFilePermissions, Define.REQUEST_FILE_PERMISSION)
                Dlog.e("checkPermissions result : $result")

                return if (result) {
                    handleUri(uri)
                }else{
                    false
                }

//                return false
//                view.loadUrl(url)
//                return false//응용프로그램이 직접 url를 처리함
            }
        }

        var mUploadMessage: ValueCallback<Uri>? = null
        var mUploadMessageArr: ValueCallback<Array<Uri>>? = null

        webView!!.webChromeClient = object : WebChromeClient() {
            override fun onJsAlert(view: WebView, url: String, message: String, result: JsResult): Boolean {
                return super.onJsAlert(view, url, message, result)
            }

            // For Android < 3.0
            fun openFileChooser(uploadMsg: ValueCallback<Uri>) {
                mUploadMessage = uploadMsg
                val i = Intent(Intent.ACTION_GET_CONTENT)
                i.addCategory(Intent.CATEGORY_OPENABLE)
                i.setType("*/*")
                startActivityForResult(Intent.createChooser(i, "파일을 선택해 주세요"), Define.REQUEST_SELECT_FILE_LEGACY)
            }

            // For Android 3.0+
            fun openFileChooser(uploadMsg: ValueCallback<Uri>, acceptType: String) {
                mUploadMessage = uploadMsg
                val i = Intent(Intent.ACTION_GET_CONTENT)
                i.addCategory(Intent.CATEGORY_OPENABLE)
                i.setType(acceptType)
                startActivityForResult(Intent.createChooser(i, "파일을 선택해 주세요"), Define.REQUEST_SELECT_FILE_LEGACY)
            }

            // For Android 4.1+
            fun openFileChooser(uploadMsg: ValueCallback<Uri>, acceptType: String, capture: String) {
                mUploadMessage = uploadMsg
                val i = Intent(Intent.ACTION_GET_CONTENT)
                i.addCategory(Intent.CATEGORY_OPENABLE)
                i.setType(acceptType)
                startActivityForResult(Intent.createChooser(i, "파일을 선택해 주세요"), Define.REQUEST_SELECT_FILE_LEGACY)
            }

            // For Android 5.0+
            @SuppressLint("NewApi")
            override fun onShowFileChooser(webView: WebView, filePathCallback: ValueCallback<Array<Uri>>, fileChooserParams: FileChooserParams): Boolean {
                if (mUploadMessageArr != null) {
                    mUploadMessageArr!!.onReceiveValue(null)
                    mUploadMessageArr = null
                }
                mUploadMessageArr = filePathCallback
                val intent = fileChooserParams.createIntent()
                try {
                    startActivityForResult(intent, Define.REQUEST_SELECT_FILE)
                } catch (e: Exception) {
                    mUploadMessageArr = null
//                    Toast.makeText(get, "에러 - 파일선택", Toast.LENGTH_LONG).show()
                    Dlog.e("에러 - 파일선택")
                    return false
                }
                return true
            }
        }

        webView!!.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {

                //This is the filter
                if (event.getAction() !== KeyEvent.ACTION_DOWN)
                    return true

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (webView!!.canGoBack()) {
                        webView!!.goBack()
                        Dlog.d("canGoBack")
                    } else {
                        Dlog.d("canNotGoBack")
                        (activity as MainActivity).onBackPressed()
                    }
                    return true
                }
                return false
            }
        })
        webView!!.setInitialScale(1)
    }

    //권한 체크
    fun checkPermissions(permissions: Array<String>, permissionResultCode: Int): Boolean {
    Dlog.e("checkPermissions")
        var result: Int
        val permissionList = ArrayList<String>()
        for (pm in permissions) {
            result = ContextCompat.checkSelfPermission(context!!, pm)
            if (result != PackageManager.PERMISSION_GRANTED) { //사용자가 해당 권한을 가지고 있지 않을 경우 리스트에 해당 권한명 추가
                permissionList.add(pm)
                Dlog.e("permissionList : $pm")
            }
        }
        if (!permissionList.isEmpty()) { //권한이 추가되었으면 해당 리스트가 empty가 아니므로 request 즉 권한을 요청합니다.
            Dlog.e("permissionList : ${permissionList}")
            requestPermissions(requireActivity(), permissionList.toTypedArray(), permissionResultCode)
            return false
        }
        return true
    }

    //권한 요청
    fun requestPermissions(requireActivity: Activity, toTypedArray: Array<String>, permissionResultCode: Int) {
        Dlog.e("requestPermissions")
        when (permissionResultCode) {
            Define.REQUEST_RECORD_VOICE_PERMISSION -> {
                for (permission in Define.needVoiceRecordPermissions) {
                    if (ActivityCompat.checkSelfPermission(context!!, permission) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(requireActivity, Define.needVoiceRecordPermissions, permissionResultCode)
                        break
                    }
                }
            }

            Define.REQUEST_RECORD_VIDEO_PERMISSION -> {
                for (permission in Define.needVideoRecordPermissions) {
                    if (ActivityCompat.checkSelfPermission(context!!, permission) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(requireActivity, Define.needVideoRecordPermissions, permissionResultCode)
                        break
                    }
                }
            }

            Define.REQUEST_FILE_PERMISSION -> {
                for (permission in Define.needVideoRecordPermissions) {
                    if (ActivityCompat.checkSelfPermission(context!!, permission) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(requireActivity, Define.needFilePermissions, permissionResultCode)
                        break
                    }
                }
            }
        }
    }
}
