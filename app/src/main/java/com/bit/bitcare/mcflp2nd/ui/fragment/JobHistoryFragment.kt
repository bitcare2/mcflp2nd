package com.bit.bitcare.mcflp2nd.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.ui.activity.MainActivity
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.Dlog
import kotlinx.android.synthetic.main.fragment_job_history.*
import android.app.Activity
import android.content.pm.PackageManager
import android.widget.Toast
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil


class JobHistoryFragment : BaseFragment(){
    private val TAG = this::class.java.simpleName

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_job_history, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        settingWebView(webview_job_history)
        Dlog.e("Define.JobHistoryUrl : "+Define.JobHistoryUrl + PreferenceUtil(activity).getStringExtra("token"))
        webview_job_history!!.loadUrl(Define.JobHistoryUrl + PreferenceUtil(activity).getStringExtra("token"))
    }

    override fun getCurrentViewName():String{
        return this::class.java.simpleName
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        var preferenceUtil: PreferenceUtil = PreferenceUtil(context)

        Dlog.e("requestCode : $requestCode")
        Dlog.e("permissions : " + permissions[0])
        Dlog.e("grantResults : " + grantResults[0])

        when (requestCode) {
//            Define.REQUEST_RECORD_VOICE_PERMISSION -> {
//                Dlog.e("onRequestVoicePermissionsResult in PopUpActivity")
//                enterRecordVoiceActivity(id!!, filegroupno!!)
//            }
//
//            Define.REQUEST_RECORD_VIDEO_PERMISSION -> {
//                Dlog.e("onRequestVideoPermissionsResult in PopUpActivity")
//                enterRecordVideoActivity(id!!, filegroupno!!)
//            }
            Define.REQUEST_FILE_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    handleUri(uri)
                } else {
                    Toast.makeText(context, "'허용' 해주셔야 다운로드가 가능합니다.", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}