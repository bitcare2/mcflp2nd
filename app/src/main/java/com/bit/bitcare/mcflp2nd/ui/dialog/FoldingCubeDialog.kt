package com.bit.bitcare.mcflp2nd.ui.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.ProgressBar
import com.bit.bitcare.mcflp2nd.R
import com.github.ybq.android.spinkit.style.Wave
import kotlinx.android.synthetic.main.activity_loading.*
import android.view.MotionEvent
import com.github.ybq.android.spinkit.style.FoldingCube


/**
 * Created by acid on 2016-12-05.
 */

class FoldingCubeDialog(context: Context) : Dialog(context) {

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_folding_cube) // 다이얼로그 레이아웃
        window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT)) //배경 투명하게
//        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
//                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL)
        val foldingCube = FoldingCube()
        spin_kit.setIndeterminateDrawable(foldingCube)
        setCancelable(true)
        setCanceledOnTouchOutside(false)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {

        if (event.action == MotionEvent.ACTION_OUTSIDE) {
            println("TOuch outside the dialog ******************** ")

        }
        return false
    }
}
