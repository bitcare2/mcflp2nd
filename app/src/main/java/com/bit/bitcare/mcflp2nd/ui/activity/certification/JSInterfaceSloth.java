package com.bit.bitcare.mcflp2nd.ui.activity.certification;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bit.bitcare.mcflp2nd.R;
import com.bit.bitcare.mcflp2nd.util.Dlog;
import com.yettiesoft.koreamint.certificate.ExtractCertificate;
import com.yettiesoft.koreamint.certificate.ManageCertificate;
import com.yettiesoft.koreamint.certificate.exception.KMException;

/**
 * Created by pol808 on 2016-12-16.
 */
public class JSInterfaceSloth {
    private AppCompatActivity context;
    private WebView webView;

    public JSInterfaceSloth(AppCompatActivity activity, WebView webView) {
        this.context = activity;
        this.webView = webView;
    }

    @JavascriptInterface
    public void setSloth(String url, final String code) {
        // 인증서 복사 로직 처리
        final ManageCertificate manager = new ManageCertificate(this.context);

        // 인증서 목록 출력 및 선택
        LayoutInflater inflater = this.context.getLayoutInflater();
        final View dialogView= inflater.inflate(R.layout.dialog_select_cert, null);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
        builder.setTitle("인증서 목록");
        builder.setView(dialogView);

        // 인증서 리스트 화면 출력
        RadioGroup group = (RadioGroup)dialogView.findViewById(R.id.dialog_rg);
        RadioButton button;
        for(int i = 0; i < manager.sizeCertificate(); i++) {
            button = new RadioButton(this.context);
            button.setText(manager.getCertificate(i).getSujectName());
            button.setId(i);
            button.setTextSize(15);
            group.addView(button);
        }

        // 선택된 인증서 비밀번호 입력 화면 출력

        // 확인 클릭 이벤트
        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RadioGroup rg= (RadioGroup) dialogView.findViewById(R.id.dialog_rg);
                int checkedId= rg.getCheckedRadioButtonId();

                EditText pinEditbox = (EditText) dialogView.findViewById(R.id.dialog_pin);
                String pin = pinEditbox.getText().toString();

                try {
                    // 선택된 인증서 구조체와 비밀번호를 입력하여 다음 함수를 호출
                    if (checkedId != -1) {
                        if (pin.length() != 0) {
                            final String outScript = ExtractCertificate.makePFX(code, pin, manager.getCertificate(checkedId));

                            webView.post(new Runnable() {
                                @Override
                                public void run() {
                                    webView.loadUrl(outScript);
                                }
                            });
                        } else {
                            Toast.makeText(context, "인증서를 비밀번호를 입력하세요.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "인증서를 선택하세요.", Toast.LENGTH_SHORT).show();
                    }
                } catch (KMException e) {
//                    Dlog.INSTANCE.e(e.toString());
                }
            }
        });

        AlertDialog dialog=builder.create();
        dialog.show();

        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = 1000;
        params.height = 800;
        dialog.getWindow().setAttributes(params);
    }
}
