package com.bit.bitcare.mcflp2nd.data.model

data class FileInfo(val paramFilegrpno: Long) {
    private var filegrpno:Long = paramFilegrpno
    private var transferType: String? = null
    private var fileType: String? = null
    private var printFileName: String? = null
    private var realfilename: String? = null
    private var fileSize: Int = 0
    private var downloadUrl: String? = "/rm/common/commonFile/tempDownload.do?filegrpno="+ filegrpno + "&realfilename=" + realfilename +"&filename=" + printFileName
}
