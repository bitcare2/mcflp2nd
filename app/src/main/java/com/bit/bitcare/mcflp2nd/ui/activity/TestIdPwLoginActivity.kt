package com.bit.bitcare.mcflp2nd.ui.activity

import android.content.Intent
import android.content.Intent.*
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.AndroidNetworking.enableLogging
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.data.model.ResponseMyInfoVo
import com.bit.bitcare.mcflp2nd.data.model.ResponseVo
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.Dlog
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil
import com.bit.bitcare.mcflp2nd.util.Util
import kotlinx.android.synthetic.main.activity_id_pw_login.*
import kotlinx.android.synthetic.main.activity_id_pw_login_test.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.toolbar_close_button.*
import org.json.JSONObject
import java.lang.Exception

class TestIdPwLoginActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_id_pw_login_test)


        Define.mobileServerIP = "http://nibp-mobile.g2e.co.kr"
        PreferenceUtil(this@TestIdPwLoginActivity).putStringExtra("mobileServerIP", "http://nibp-mobile.g2e.co.kr")
        Dlog.e("Define.mobileServerIP: " + Define.mobileServerIP)


        id_pw_login_title.setOnClickListener {
            val intent = Intent(this, TestIpChangeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        id_pw_login_button_test.setOnClickListener {
            Dlog.e("url: " + Define.mobileServerIP)
            Dlog.e("id : " + id_pw_login_id_test.text.toString())

            waveDialog!!.show()

            AndroidNetworking.get(Define.mobileServerIP + "/cert/login")
                    .addQueryParameter("userId", id_pw_login_id_test.text.toString())
                    .addQueryParameter("userPw", "1")
                    .addQueryParameter("prvcertdn", "1")
                    .addHeaders("Content-Type","text/plain; charset=utf-8")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject) {
                            Dlog.e("받아옴??")
                            Dlog.e(response.toString())
                            var loginVo = gson.fromJson(response.toString(), ResponseVo::class.java)
                            Dlog.e(loginVo.toString())
                            if(loginVo.result.token==null){
                                viewError(loginVo.serviceMsg)
                            }else{
                                PreferenceUtil(this@TestIdPwLoginActivity).putStringExtra("token",loginVo.result.token)
                                getMyInfo()
                            }
                        }

                        override fun onError(error: ANError) {
                            // handle error
                            Dlog.e("에러??" )
                            waveDialog!!.dismiss()
                            Dlog.e(error.errorCode.toString())
                            Dlog.e(error.errorBody)
                            Dlog.e(error.errorDetail)
                        }
                    })
        }
    }

    fun getMyInfo() {
        AndroidNetworking.get(Define.mobileServerIP + "/comm/getMyInfoBySn")
                .addQueryParameter("token", PreferenceUtil(this@TestIdPwLoginActivity).getStringExtra("token"))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        try {
                            Dlog.e("받아옴?")
                            Dlog.e(response.toString())

                            var responseMyInfoVo = gson.fromJson(response.toString(), ResponseMyInfoVo::class.java)
                            Dlog.e(responseMyInfoVo.toString())
                            waveDialog!!.dismiss()
                            if("100".equals(responseMyInfoVo.serviceCode)){
                                if(responseMyInfoVo.result.token==null){
                                    viewError(responseMyInfoVo.serviceMsg)
                                }else{
                                    PreferenceUtil(this@TestIdPwLoginActivity).putStringExtra("token",responseMyInfoVo.result.token)
                                    if(responseMyInfoVo.result.loginInfo != null){
                                        PreferenceUtil(this@TestIdPwLoginActivity).putStringExtra("usernm",responseMyInfoVo.result.loginInfo.usernm)

                                        val intent = Intent(this@TestIdPwLoginActivity, MainActivity::class.java)
                                        intent.addFlags(Intent. FLAG_ACTIVITY_CLEAR_TOP)
                                        intent.addFlags(Intent. FLAG_ACTIVITY_CLEAR_TASK)
                                        intent.addFlags(Intent. FLAG_ACTIVITY_NEW_TASK)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                        startActivity(intent)
                                        overridePendingTransition(0,0)
                                    }
                                }
                            }else{
                                Toast.makeText(this@TestIdPwLoginActivity, "에러코드 : "+ responseMyInfoVo.serviceCode + "\n에러메세지 : "+ responseMyInfoVo.serviceMsg ,Toast.LENGTH_LONG).show()
                            }
                        }catch (e: Exception){
                            waveDialog!!.dismiss()
                            Toast.makeText(this@TestIdPwLoginActivity, "로그인 에러\n관리자에게 문의해주세요.",Toast.LENGTH_LONG).show()
                        }
                    }
                    override fun onError(error: ANError) {
                        waveDialog!!.dismiss()
                        Toast.makeText(this@TestIdPwLoginActivity, "통신 에러\n관리자에게 문의해주세요.",Toast.LENGTH_LONG).show()
                    }
                })
    }
    

    fun viewError(message: String) {
        MaterialDialog(this)
                .title(R.string.fail_login)
                .message(text = message)
                .positiveButton (R.string.done){

                }
                .show()
    }
}