package com.bit.bitcare.mcflp2nd.ui.activity.media

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.PersistableBundle
import android.support.design.widget.TabLayout
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.checkbox.checkBoxPrompt
import com.afollestad.materialdialogs.checkbox.isCheckPromptChecked
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.StringRequestListener
import com.androidnetworking.interfaces.UploadProgressListener
import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.ui.activity.BaseActivity
import com.bit.bitcare.mcflp2nd.ui.activity.LoginActivity
import com.bit.bitcare.mcflp2nd.ui.dialog.FoldingCubeDialog
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.Dlog
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil
import com.bit.bitcare.mcflp2nd.util.Util
import kotlinx.android.synthetic.main.activity_camera_media.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class CameraMediaActivity : BaseActivity() {
    private var willSendFile: File? = null
    private var responseString: String? = null
    private var filegrpno: String? = null
    private var foldingCubeDialog: FoldingCubeDialog? = null
//
    private val outputMediaFile: File?
        get() {
    //            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED, ignoreCase = true)) {
            val filedir = File(Environment.getExternalStorageDirectory(), "NIBP")
            if (!filedir.exists()) {
                filedir.mkdir()
            }
            val currenttime = SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA).format(Date())
            return File(filedir.absolutePath + File.separator + "Video_"+ currenttime + ".mp4")
    //            }
            return null
        }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        var permissionToRecordAccepted = true

        when (requestCode) {
            Define.REQUEST_RECORD_VIDEO_PERMISSION -> for (result in grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    permissionToRecordAccepted = false
                    break
                }
            }
        }

        if (permissionToRecordAccepted == false) {
            exit()
        }
    }

//    override fun setRequestedOrientation(requestedOrientation: Int) {
//        /*
//         if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
//             // no-op
//         }else{
//             super.setRequestedOrientation(requestedOrientation);
//         }
//         */
//        try {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        } catch (e: IllegalStateException) {
//
//        }
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_camera_media)
        setFinishOnTouchOutside(false)

        willSendFile = outputMediaFile

        foldingCubeDialog = FoldingCubeDialog(this)

        filegrpno = intent?.extras?.getString("filegrpno")

        for (permission in Define.needVideoRecordPermissions) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, Define.needVideoRecordPermissions, Define.REQUEST_RECORD_VIDEO_PERMISSION)
                break
            }
        }

        record_button.setOnClickListener{
            val intent = Intent(this@CameraMediaActivity, RecordActivity::class.java)
            startActivityForResult(intent, Define.REQUEST_RECORD_VIDEO_CAPTURE)
        }

        record_button_icon.setOnClickListener {
            val intent = Intent(this@CameraMediaActivity, RecordActivity::class.java)
            startActivityForResult(intent, Define.REQUEST_RECORD_VIDEO_CAPTURE)
        }

        play_button.setOnClickListener {
            if (willSendFile!!.exists()) {
                val intent = Intent(this@CameraMediaActivity, PlayActivity::class.java)
                intent.putExtra("willSendFile", willSendFile!!.absolutePath)
                startActivity(intent)
            } else {
                Toast.makeText(this, "녹화된 영상이 없습니다.", Toast.LENGTH_LONG).show()
            }
        }

        play_button_icon.setOnClickListener {
            if (willSendFile!!.exists()) {
                val intent = Intent(this@CameraMediaActivity, PlayActivity::class.java)
                intent.putExtra("willSendFile", willSendFile!!.absolutePath)
                startActivity(intent)
            } else {
                Toast.makeText(this, "녹화된 영상이 없습니다.", Toast.LENGTH_LONG).show()
            }
        }

        upload_button.setOnClickListener {
            if (willSendFile!!.exists()) {
                if (willSendFile != null) {
                    sendFile(willSendFile!!)
                } else {
                    //파일삭제
                    if (willSendFile!!.exists()) {
                        willSendFile!!.delete()
                    }
                    Toast.makeText(this, "녹화를 다시 해주세요.", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this, "녹화된 영상이 없습니다.", Toast.LENGTH_LONG).show()
            }
        }

        upload_button_icon.setOnClickListener {
            if (willSendFile!!.exists()) {
                if (willSendFile != null) {

                    if(PreferenceUtil(this).getBooleanExtra("wifi_check")){
                        Dlog.e("그냥 보냄")
                        sendFile(willSendFile!!)
                    }else{
                        var fileSize = Util.getFileSize(willSendFile!!.length().toString())
                        MaterialDialog(this)
                                .title(text = fileSize)
                                .message(R.string.wifi_check)
                                .positiveButton (R.string.yes){dialog ->
                                    if(dialog.isCheckPromptChecked()){
                                        PreferenceUtil(this).putBooleanExtra("wifi_check",true)
                                        Dlog.e("체크 저장")
                                    }
                                    sendFile(willSendFile!!)
                                }
                                .negativeButton(R.string.no){

                                }
                                .checkBoxPrompt(R.string.do_not_check) { checked ->
                                    // Check box was checked or unchecked
                                }
                                .show()
                    }
                } else {
                    //파일삭제
                    if (willSendFile!!.exists()) {
                        willSendFile!!.delete()
                    }
                    Toast.makeText(this, "녹화를 다시 해주세요.", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this, "녹화된 영상이 없습니다.", Toast.LENGTH_LONG).show()
            }
        }

        close_button.setOnClickListener{
            exit()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.e("onActivityResult", "onActivityResult $requestCode, $resultCode")
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                Define.REQUEST_RECORD_VIDEO_CAPTURE -> {
                    try {
                        willSendFile = File(data!!.getStringExtra("result"))
                    } catch (e: Exception) {
                        Toast.makeText(this, "영상이 정상적으로 저장되지 못했습니다. \n다시 녹화해 주세요", Toast.LENGTH_LONG)
                    }
                }
            }
        }
    }

    private fun sendFile(file: File) {
        Dlog.e("sendFile")
        Dlog.e("file : " + file.absolutePath)

        val uploadProgressListener = UploadProgressListener { bytesUploaded, totalBytes ->
            Dlog.e("send file onProgress : " + bytesUploaded.toString() + "%")
            var per: Long = bytesUploaded * 100 / totalBytes
            Dlog.e("send file onProgress : " + per.toString() + "%")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                progress_bar_record_video.setProgress(per.toInt(), true)
            }else{
                foldingCubeDialog?.show()
            }
        }

        Dlog.e("upload url : $" + Define.mobileServerIP + "/temp/file/upload")

        AndroidNetworking.upload(Define.mobileServerIP + "/temp/file/upload")
                .addMultipartFile("file1", file)
                .addMultipartParameter("filegrpno", filegrpno!!.toString())
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(uploadProgressListener)
                .getAsString(stringRequestListener)

    }

    private val stringRequestListener = object : StringRequestListener {
        override fun onResponse(response: String) {
            Dlog.e("send file onResponse : " + response.toString())
            responseString = response.toString()

            foldingCubeDialog?.dismiss()

            //파일삭제
            if (willSendFile!!.exists()) {
                willSendFile!!.delete()
            }

            var resultIntent = Intent()
            resultIntent.putExtra("result", responseString)
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        }

        override fun onError(error: ANError) {
            foldingCubeDialog?.dismiss()

            if (error.errorCode !== 0) {
                // received error from server
                // error.getErrorCode() - the error code from server
                // error.getErrorBody() - the error body from server
                // error.getErrorDetail() - just an error detail
                Log.d("send file onError", "onError errorCode : " + error.errorCode)
                Log.d("send file onError", "onError errorBody : " + error.errorBody)
                Log.d("send file onError", "onError errorDetail : " + error.errorDetail)
                // get parsed error object (If ApiError is your class)
//                    val apiError = error.getErrorAsObject(ApiError::class.java)
            } else {
                // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                Log.d("send file onError", "onError errorDetail : " + error.errorDetail)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (willSendFile!!.exists()) {
            willSendFile!!.delete()
        }
    }

    override fun onBackPressed() {
        exit()
    }

    private fun exit(){
        if(willSendFile!!.exists()) {
            MaterialDialog(this)
                    .title(R.string.camera_exit)
                    .message(R.string.camera_exit_message)
                    .positiveButton (R.string.yes){
                        finish()
                    }
                    .negativeButton(R.string.no){

                    }
                    .show()
        }else{
            finish()
        }
    }
}
