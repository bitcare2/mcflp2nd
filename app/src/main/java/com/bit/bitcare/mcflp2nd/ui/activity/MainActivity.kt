package com.bit.bitcare.mcflp2nd.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.widget.Toolbar
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bit.bitcare.mcflp2nd.BaseApplication
import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.data.model.ResponseVo
import com.bit.bitcare.mcflp2nd.ui.adapter.MainPagerAdapter
import com.bit.bitcare.mcflp2nd.ui.fragment.JobHistoryFragment
import com.bit.bitcare.mcflp2nd.ui.fragment.TemporarySaveFragment
import com.bit.bitcare.mcflp2nd.ui.fragment.UserSearchFragment
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.Dlog
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject


class MainActivity : BaseActivity() {
    companion object {
        private var tabPosition = 0
    }
    var preferenceUtil: PreferenceUtil? = null
    var menuList:List<Map<String,String>>? = null
    var pageCount = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar_main as Toolbar?)

        preferenceUtil = PreferenceUtil(baseContext)

        toolbar_main_title.text = resources.getString(R.string.title_search)

        try {
            if (BaseApplication.sw >= 600) {
                toolbar_button_logout.setOnClickListener {
                    logout()
                }

                toolbar_button_refresh.setOnClickListener {
                    when (tabPosition) {
                        0 -> {
                            Dlog.e("userSearchFragment?.webView!!.reload()")
                            val tag = preferenceUtil!!.getStringExtra("UserSearchFragmentTag")
                            val userSearchFragment = supportFragmentManager.findFragmentByTag(tag) as UserSearchFragment
                            userSearchFragment?.webView!!.reload()
//                        updateToken()
                        }
                        1 -> {
                            Dlog.e("temporarySaveFragment?.webView!!.reload()")
                            val tag = preferenceUtil!!.getStringExtra("TemporarySaveFragmentTag")
                            val temporarySaveFragment = supportFragmentManager.findFragmentByTag(tag) as TemporarySaveFragment
                            temporarySaveFragment?.webView!!.reload()
//                        updateToken()
                        }
                        2 -> {
                            //                    Dlog.e("jobListFragment?.webView!!.reload()")
                            //                    val tag = preferenceUtil!!.getStringExtra("JobListFragmentTag")
                            //                    val jobListFragment = supportFragmentManager.findFragmentByTag(tag) as JobListFragment
                            //                    jobListFragment?.webView!!.reload()
                            Dlog.e("jobHistoryFragment?.webView!!.reload()")
                            val tag = preferenceUtil!!.getStringExtra("JobHistoryFragmentTag")
                            val jobHistoryFragment = supportFragmentManager.findFragmentByTag(tag) as JobHistoryFragment
                            jobHistoryFragment?.webView!!.reload()
//                        updateToken()
                        }
                        //                3 -> {
                        //                    Dlog.e("jobHistoryFragment?.webView!!.reload()")
                        //                    val tag = preferenceUtil!!.getStringExtra("JobHistoryFragmentTag")
                        //                    val jobHistoryFragment = supportFragmentManager.findFragmentByTag(tag) as JobHistoryFragment
                        //                    jobHistoryFragment?.webView!!.reload()
                        //
                        //                }
                    }
                }
            }
        }catch (e:java.lang.Exception){
            Toast.makeText(this@MainActivity, "관리자에게 문의해주세요.", Toast.LENGTH_LONG).show()
        }
//        //관리자 인지 확인
//        val isAdmin = preferenceUtil!!.getStringExtra("isAdmin")
//
//        if("Y".equals(isAdmin)){
//            pageCount = 2
//        }else{
            pageCount = 3
//        }

        updateToken()

    }

    open fun initTitle(title:String){
        toolbar_main_title.text = title
    }

    fun setViewpager(){
        val fragmentAdapter = MainPagerAdapter(supportFragmentManager, this, pageCount)
        viewpager_main.adapter = fragmentAdapter
        tabs_main.setupWithViewPager(viewpager_main)
        viewpager_main.setPagingEnabled(true) //ViewPager Swipe On/Off
        viewpager_main.offscreenPageLimit = 3

        tabs_main.addOnTabSelectedListener(addOnTabSelectedListener)
        tabPosition = 0
    }

    open fun updateToken(){
        Dlog.e("updateToken()")
        AndroidNetworking.get(Define.mobileServerIP+"/token")
                .addQueryParameter("token", PreferenceUtil(this@MainActivity).getStringExtra("token"))
                .addHeaders("Content-Type","text/plain; charset=utf-8")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Dlog.e("token 받아옴")
                        var responseVo = gson.fromJson(response.toString(), ResponseVo::class.java)
                        Dlog.e("받아온 토큰 : "+responseVo.result.token )
                        PreferenceUtil(this@MainActivity).putStringExtra("token",responseVo.result.token)
                        setViewpager()
                    }

                    override fun onError(error: ANError) {
                        // handle error
                        Dlog.e("token 받기 에러")
                    }
                })
    }

    override fun onResume() {
        super.onResume()
        Dlog.e("onResume")
    }
    override fun onStart() {
        super.onStart()
        Dlog.e("onStart")

    }

    private var addOnTabSelectedListener = object: TabLayout.OnTabSelectedListener {
        override fun onTabSelected(p0: TabLayout.Tab?) {
            tabPosition = p0!!.position
            Dlog.e("현재 탭 $tabPosition")
            when(tabPosition){
                0 -> {
                    initTitle(resources.getString(R.string.title_search))
                }
                1 -> {
                    initTitle(resources.getString(R.string.title_temporary_save))
                }
                2 -> {
                    initTitle(resources.getString(R.string.title_job_history))
                }
            }
        }

        override fun onTabReselected(p0: TabLayout.Tab?) {
//            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onTabUnselected(p0: TabLayout.Tab?) {
//            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val turnsType = object : TypeToken<List<Map<String, String>>>(){}.type
        menuList = gson.fromJson<List<Map<String,String>>>(PreferenceUtil(this).getStringExtra("menuList")!!,turnsType!!)

        try{
            for ((i, menuvo) in menuList!!.withIndex()) {
                menu.add(0, i, Menu.NONE, menuvo.get("title"))
                Dlog.e("menuvo.count() = "+i)
            }
        }catch (e:Exception){
            Dlog.e("e.message : $e.message")
        }

        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        try{
            var menuvo = menuList!!.get(item.itemId)

            if(menuvo.get("url")!=""){
                val intent = Intent(this, PopUpActivity::class.java)
                var bundle: Bundle = Bundle().apply {
                    putString("title", menuvo.get("title"))
                    putString("url", menuvo.get("url"))
                    putString("option", PreferenceUtil(this@MainActivity).getStringExtra("token"))
                }
                intent.putExtras(bundle)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }else{
                var activityInfo = menuvo.get("option")
                var activityClass = Class.forName(activityInfo)
                val intent = Intent(this, activityClass)
                startActivity(intent)
                overridePendingTransition(0,0)
            }
        }catch (e:Exception){
            Dlog.e("e.message : $e.message")
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        finishApp()
    }

    fun View.hideKeyboard() {
        (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(windowToken, 0)

    }

    private fun logout(){
        MaterialDialog(this)
                .title(R.string.do_logout_title)
                .message(R.string.do_logout_message)
                .positiveButton (R.string.yes){
                    Dlog.e("로그아웃 - 예")
                    PreferenceUtil(this@MainActivity).clear()
//                    PreferenceUtil(this@MainActivity).putStringExtra("serverIP", "https://intra.lst.go.kr")
//                    PreferenceUtil(this@MainActivity).putStringExtra("mobileServerIP", "https://mobile.lst.go.kr")

                    val intent = Intent(this, SplashActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                    startActivity(intent)
                    overridePendingTransition(0,0)
                }
                .negativeButton(R.string.no){
                    Dlog.e("로그아웃 - 아니오")
                }
                .show()
    }

    private fun finishApp(){
        MaterialDialog(this)
                .title(R.string.do_exit_title)
                .message(R.string.do_exit_message)
                .positiveButton (R.string.yes){
                    Dlog.e("종료 - 예")
                    finish()
                }
                .negativeButton(R.string.no){
                    Dlog.e("종료 - 아니오")
                }
                .show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        Dlog.e("onRequestPermissionsResult's Position : $tabPosition")

        when (tabPosition) {
            0->{
                val tag =  preferenceUtil!!.getStringExtra("UserSearchFragmentTag")
                val userSearchFragment = supportFragmentManager.findFragmentByTag(tag) as UserSearchFragment
                userSearchFragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
            1->{
                val tag =  preferenceUtil!!.getStringExtra("TemporarySaveFragmentTag")
                val temporarySaveFragment = supportFragmentManager.findFragmentByTag(tag) as TemporarySaveFragment
                temporarySaveFragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
            2->{
//                val tag =  preferenceUtil!!.getStringExtra("JobListFragmentTag")
//                val jobListFragment = supportFragmentManager.findFragmentByTag(tag)  as JobListFragment
//                jobListFragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
                val tag =  preferenceUtil!!.getStringExtra("JobHistoryFragmentTag")
                val jobHistoryFragment = supportFragmentManager.findFragmentByTag(tag) as JobHistoryFragment
                jobHistoryFragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
//            3->{
//                val tag =  preferenceUtil!!.getStringExtra("JobHistoryFragmentTag")
//                val jobHistoryFragment = supportFragmentManager.findFragmentByTag(tag) as JobHistoryFragment
//                jobHistoryFragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
//            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        var preferenceUtil:PreferenceUtil = PreferenceUtil(baseContext)

        Dlog.e("resultCode = $resultCode")
        Dlog.e("requestCode = $requestCode")
        Dlog.e("data = ${data.toString()}")
        Dlog.e("tabPosition = $tabPosition")

        when (tabPosition) {
            0 -> {
                val tag = preferenceUtil.getStringExtra("UserSearchFragmentTag")
                val userSearchFragment = supportFragmentManager.findFragmentByTag(tag) as UserSearchFragment
                userSearchFragment?.onActivityResult(requestCode, resultCode, data)
            }
            1 -> {
                val tag = preferenceUtil.getStringExtra("TemporarySaveFragmentTag")
                val temporarySaveFragment = supportFragmentManager.findFragmentByTag(tag) as TemporarySaveFragment
                temporarySaveFragment?.onActivityResult(requestCode, resultCode, data)
            }
            2 -> {
//                val tag = preferenceUtil.getStringExtra("JobListFragmentTag")
//                val jobListFragment = supportFragmentManager.findFragmentByTag(tag) as JobListFragment
//                jobListFragment?.onActivityResult(requestCode, resultCode, data)
                val tag = preferenceUtil.getStringExtra("JobHistoryFragmentTag")
                val jobHistoryFragment = supportFragmentManager.findFragmentByTag(tag) as JobHistoryFragment
                jobHistoryFragment?.onActivityResult(requestCode, resultCode, data)
            }
//            3 -> {
//                val tag = preferenceUtil.getStringExtra("JobHistoryFragmentTag")
//                val jobHistoryFragment = supportFragmentManager.findFragmentByTag(tag) as JobHistoryFragment
//                jobHistoryFragment?.onActivityResult(requestCode, resultCode, data)
//            }
        }
    }
}


