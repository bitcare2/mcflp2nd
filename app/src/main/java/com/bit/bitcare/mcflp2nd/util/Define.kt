package com.bit.bitcare.mcflp2nd.util

import android.Manifest
import com.bit.bitcare.mcflp2nd.ui.activity.LoginActivity

class Define {
    companion object {
//        var serverIP = "mobile.lst.go.kr:8080"
        var serverIP = "https://intra.lst.go.kr" //실서버
//        var serverIP = "59.10.164.89:8080" //임시파일업로드 및 메뉴
//        var bitLocalServerIP = "http://59.10.164.89:8080" //테스트서버

        var appSetupInfoServerIP = "https://mobile.lst.go.kr" //앱설정 API 용 - 이건 고정 무조건 실서버 봄

        var mobileServerIP = "https://mobile.lst.go.kr" //모바일 API 서버 : 로그인 임시파일업로드 및 메뉴
//        var mobileServerIP = "59.10.164.89:8080" //임시파일업로드 및 메뉴

        var ozEndPoint = "https://report.lst.go.kr:443/oz70/server"
//        var ozEndPoint = "http://59.10.164.94:8080/oz70/server"
//        var ozEndPoint = "http://14.36.46.131:8480/oz70/server"

        var userSearchUrl               = "$serverIP/mobile/reg/mng/orgWrtMng/index.do?tabFlag=A&token="
        var temporarySaveListUrl        = "$serverIP/mobile/reg/mng/orgWrtMng/index.do?tabFlag=B&token="
        var JobHistoryUrl               = "$serverIP/mobile/reg/mng/orgWrtMng/index.do?tabFlag=C&token="
        var certLoginUrl                = "$serverIP/rm/mobile/mobilecert.do"

        const val REQUEST_RECORD_VOICE_PERMISSION = 1001
        const val REQUEST_RECORD_VIDEO_PERMISSION = 1002
        const val REQUEST_FILE_PERMISSION = 1003
        const val REQUEST_SELECT_FILE_LEGACY = 2001
        const val REQUEST_SELECT_FILE = 2002
        const val REQUEST_RECORD_SOUND_ACTION = 3001
        const val REQUEST_RECORD_VIDEO_CAPTURE = 3002
        const val REQUEST_SEARCH_JUSO = 4001
        const val REQUEST_REG_FORM = 4002

        const val VIDEO_RECORD_TIME = 300
        const val VIDEO_RECORD_MILLI_TIME:Long = VIDEO_RECORD_TIME.toLong() * 1000 + 100

        val needVoiceRecordPermissions = arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val needVideoRecordPermissions = arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val needFilePermissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }
}