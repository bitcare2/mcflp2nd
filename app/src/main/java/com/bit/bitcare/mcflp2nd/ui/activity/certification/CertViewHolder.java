package com.bit.bitcare.mcflp2nd.ui.activity.certification;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bit.bitcare.mcflp2nd.R;
import com.yettiesoft.koreamint.certificate.pkcs.CertInfo;

/**
 * Created by pol808 on 2016-08-26.
 */
public class CertViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    public final TextView mSubject;
    public final TextView mIssue;
    public final TextView mValidTo;
    public final ImageView mExpire;
    public final ImageView mCertConfirm;

    public CertInfo mItem;

    //////////////////////////////////////////////////////////////
    // 인증서 리스트 항목 viewer
    public CertViewHolder (View view) {
        super(view);
        mView = view;

        mSubject = (TextView) view.findViewById(R.id.subject);
        mIssue = (TextView) view.findViewById(R.id.issue);
        mValidTo = (TextView) view.findViewById(R.id.validTo);
        mExpire = (ImageView) view.findViewById(R.id.expireIcon);

        mCertConfirm = (ImageView)view.findViewById(R.id.certConfirm);
        mCertConfirm.setVisibility(View.GONE);
    }

    public void setUnVisible() {
        mCertConfirm.setVisibility(View.GONE);
    }

    public void setVisible() {
        mCertConfirm.setVisibility(View.VISIBLE);
    }

    @Override
    public String toString() {
        return super.toString() + " '" + mSubject.getText() + "'";
    }

}
