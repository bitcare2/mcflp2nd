package com.bit.bitcare.mcflp2nd.ui.fragment

import android.content.Context
import android.support.v4.widget.SwipeRefreshLayout
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ViewGroup
import android.widget.ListView
import android.support.v4.view.ViewCompat.canScrollVertically
import android.support.v4.widget.NestedScrollView


class CustSwipeRefreshLayout : SwipeRefreshLayout {
    var viewGroup: ViewGroup? = null
    var nestedScrollView: NestedScrollView? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

//    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
//        return if (null != viewGroup) {
//            viewGroup!!.scrollY <= 1 && super.onInterceptTouchEvent(ev)
//        } else super.onInterceptTouchEvent(ev)
//    }

//    fun setScrollChild(nestedScrollView: NestedScrollView) {
//        this.nestedScrollView = nestedScrollView
//    }
//
//    override fun canChildScrollUp(): Boolean {
//        return nestedScrollView != null && nestedScrollView!!.canScrollVertically(-1) || super.canChildScrollUp()
//    }

    private var mCanChildScrollUpCallback: CanChildScrollUpCallback? = null

    interface CanChildScrollUpCallback {
        fun canSwipeRefreshChildScrollUp(): Boolean
    }

    fun setCanChildScrollUpCallback(canChildScrollUpCallback: CanChildScrollUpCallback) {
        mCanChildScrollUpCallback = canChildScrollUpCallback
    }

    override fun canChildScrollUp(): Boolean {
        return if (mCanChildScrollUpCallback != null) {
            mCanChildScrollUpCallback!!.canSwipeRefreshChildScrollUp()
        } else super.canChildScrollUp()
    }
}