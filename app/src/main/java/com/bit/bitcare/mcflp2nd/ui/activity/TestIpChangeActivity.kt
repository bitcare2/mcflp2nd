package com.bit.bitcare.mcflp2nd.ui.activity

import android.content.Intent
import android.content.Intent.*
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.AndroidNetworking.enableLogging
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.data.model.ResponseMyInfoVo
import com.bit.bitcare.mcflp2nd.data.model.ResponseVo
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.Dlog
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil
import com.bit.bitcare.mcflp2nd.util.Util
import kotlinx.android.synthetic.main.activity_id_pw_login.*
import kotlinx.android.synthetic.main.activity_id_pw_login_test.*
import kotlinx.android.synthetic.main.activity_ip_change_test.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.toolbar_close_button.*
import org.json.JSONObject
import java.lang.Exception

class TestIpChangeActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ip_change_test)

        var serverIp: String = PreferenceUtil(this@TestIpChangeActivity).getStringExtra("serverIp")

        if(serverIp.equals("")){
            ip_change_server_ip_test.setText("http://nibp-intra.g2e.co.kr")
        }else{
            ip_change_server_ip_test.setText(serverIp)
            Define.serverIP = serverIp
            Define.userSearchUrl               = "$serverIp/mobile/reg/mng/orgWrtMng/index.do?tabFlag=A&token="
            Define.temporarySaveListUrl        = "$serverIp/mobile/reg/mng/orgWrtMng/index.do?tabFlag=B&token="
            Define.JobHistoryUrl               = "$serverIp/mobile/reg/mng/orgWrtMng/index.do?tabFlag=C&token="
//            Toast.makeText(this@TestIpChangeActivity, "serverip : $serverIp",Toast.LENGTH_LONG).show()
        }

        ip_change_button_test.setOnClickListener {
            val serverIp = ip_change_server_ip_test.text.toString().trim()
            Define.serverIP = serverIp
            Define.userSearchUrl               = "$serverIp/mobile/reg/mng/orgWrtMng/index.do?tabFlag=A&token="
            Define.temporarySaveListUrl        = "$serverIp/mobile/reg/mng/orgWrtMng/index.do?tabFlag=B&token="
            Define.JobHistoryUrl               = "$serverIp/mobile/reg/mng/orgWrtMng/index.do?tabFlag=C&token="
            PreferenceUtil(this@TestIpChangeActivity).putStringExtra("serverIP",serverIp)
            Toast.makeText(this@TestIpChangeActivity, "IP가 "+ Define.serverIP + "로 설정되었습니다." ,Toast.LENGTH_LONG).show()
            finish()
        }


    }

}