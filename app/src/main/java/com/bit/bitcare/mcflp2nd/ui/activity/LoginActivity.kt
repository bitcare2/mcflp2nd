package com.bit.bitcare.mcflp2nd.ui.activity

import android.content.Intent
import android.content.Intent.*
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.androidnetworking.interfaces.StringRequestListener
import com.bit.bitcare.mcflp2nd.BuildConfig
import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.data.model.MenuVo
import com.bit.bitcare.mcflp2nd.data.model.ResponseVo
import com.bit.bitcare.mcflp2nd.ui.activity.certification.CertificationActivity
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.Dlog
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil
import com.bit.bitcare.mcflp2nd.util.Util
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_id_pw_login.*
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject

class LoginActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        login_certification_button.setOnClickListener {
            //하루에 한번만 공인인증서 로그인 하도록 함
            var loginType = PreferenceUtil(this@LoginActivity).getStringExtra("LoginType")
            var todate = PreferenceUtil(this@LoginActivity).getStringExtra("todate")

            if(todate.equals(Util.getToDate())&&loginType.equals(CertificationActivity.MED)){
                //의료기관
                var bundle: Bundle = Bundle().apply {
                    putString("LoginType", loginType)
                }
                var dnValue = PreferenceUtil(this@LoginActivity).getStringExtra("dn")
                bundle.putString("dn", dnValue)

                var pkiClsCode = PreferenceUtil(this@LoginActivity).getStringExtra("pkiClsCode")
                bundle.putString("pkiClsCode", pkiClsCode)

                val intent = Intent(baseContext, IdPwLoginActivity::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
                overridePendingTransition(0,0)
                finish()
            }else{
                //상담(등록)기관
                val intent = Intent(this, CertificationActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                startActivity(intent)
                overridePendingTransition(0,0)
            }
        }

        if(BuildConfig.FLAVOR.equals("demo")) {
            login_certification_title.setOnClickListener {
                val intent = Intent(this, TestIdPwLoginActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                startActivity(intent)
                overridePendingTransition(0,0)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        getMenuList()
    }

    var menuList:List<Map<String,String>>? = null

    fun getMenuList(){
        Dlog.e("getMenuList")
        AndroidNetworking.post(Define.mobileServerIP+"/temp/menu")
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(stringRequestListener)
    }

    val stringRequestListener = object: StringRequestListener {
        override fun onResponse(response:String) {
            Dlog.e("stringRequestListener : " + response)

            var menuList: MutableList<MenuVo> = gson.fromJson(response, object: TypeToken<ArrayList<MenuVo>>(){}.type)
            Dlog.e("menuList : " + gson.toJson(menuList))
            menuList.plusAssign(MenuVo("앱정보","", "com.bit.bitcare.mcflp2nd.ui.activity.AppInfoActivity"))
            Dlog.e("menuList : " + gson.toJson(menuList))
            PreferenceUtil(this@LoginActivity).putStringExtra("menuList", gson.toJson(menuList))

        }
        override fun onError(error: ANError) { 
            if (error.errorCode !== 0) {
                // received error from server
                // error.getErrorCode() - the error code from server
                // error.getErrorBody() - the error body from server
                // error.getErrorDetail() - just an error detail
                Dlog.d("onError errorCode : " + error.errorCode)
                Dlog.d("onError errorBody : " + error.errorBody)
                Dlog.d("onError errorDetail : " + error.errorDetail)
            } else {
                // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                Dlog.d("onError errorDetail : " + error.errorDetail)
            }
        }
    }
}