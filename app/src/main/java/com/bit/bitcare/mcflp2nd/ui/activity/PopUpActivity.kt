package com.bit.bitcare.mcflp2nd.ui.activity

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.DownloadManager
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Build.VERSION.SDK_INT
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.webkit.*
import android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
import android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.view.View
import android.view.WindowManager
import android.webkit.WebView
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.StringRequestListener
import com.androidnetworking.interfaces.UploadProgressListener
import com.bit.bitcare.mcflp2nd.BaseApplication
import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.ui.activity.media.AudioMediaActivity
import com.bit.bitcare.mcflp2nd.ui.activity.media.CameraMediaActivity
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.Define.Companion.REQUEST_SEARCH_JUSO
import com.bit.bitcare.mcflp2nd.util.Define.Companion.REQUEST_SELECT_FILE
import com.bit.bitcare.mcflp2nd.util.Define.Companion.REQUEST_SELECT_FILE_LEGACY
import com.bit.bitcare.mcflp2nd.util.Dlog
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil
import kotlinx.android.synthetic.main.activity_pop_up.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar_refresh_close_button.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class PopUpActivity : BaseActivity() {
    private lateinit var url: String
    private lateinit var option: String
    private var id:String? = null
    private var filegroupno:String? = null
    private var dialog:AlertDialog? = null
    private var webPageLoadSuccessFlag = "N"
    private lateinit var downloadUrl: String
    private lateinit var downloadUserAgent: String
    private lateinit var downloadContentDisposition: String
    private lateinit var downloadMimeType: String

    inner class BitInterface {
        @JavascriptInterface
        fun reqRecVoice(fileuploadId: String, filegrpno: String) { //녹음 요청
            Dlog.e("fileuploadId = " + fileuploadId)
            Dlog.e("filegrpno = " + filegrpno)
            id = fileuploadId
            filegroupno = filegrpno

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                var isPassed = checkPermissions(Define.needVoiceRecordPermissions, Define.REQUEST_RECORD_VOICE_PERMISSION)
                if (isPassed) {
                    enterRecordVoiceActivity(fileuploadId, filegrpno)
                } else {
                    //                    Toast.makeText(getApplicationContext(), "권한 동의를 해주셔야 사용이 가능합니다.", Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(getApplicationContext(), "본 기기에서는 해당 기능을 이용할 수 없습니다.", Toast.LENGTH_SHORT).show()
            }
        }

        @JavascriptInterface
        fun reqRecVod(fileuploadId: String, filegrpno: String) { //녹화 요청
            Dlog.e("fileuploadId = " + fileuploadId)
            Dlog.e("filegrpno = " + filegrpno)
            id = fileuploadId
            filegroupno = filegrpno

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                var isPassed = checkPermissions(Define.needVideoRecordPermissions, Define.REQUEST_RECORD_VIDEO_PERMISSION)
                if (isPassed) {
                    enterRecordVideoActivity(fileuploadId, filegrpno)
                }
            }else{
                Toast.makeText(getApplicationContext(), "본 기기에서는 해당 기능을 이용할 수 없습니다.", Toast.LENGTH_SHORT).show()
            }
        }

        @JavascriptInterface
        fun getToken(): String { //토큰 요청
            var token = PreferenceUtil(this@PopUpActivity).getStringExtra("token")
            Dlog.e("token : $token")
            return token
        }

        @JavascriptInterface
        fun openWebView(title: String, url: String, option: String) {
            Dlog.e("openPopup / $title / $url / $option")
            val intent = Intent(baseContext, PopUpActivity::class.java)
            var bundle: Bundle = Bundle().apply {
                putString("title", title)
                putString("url", url)
                putString("option", option)
            }
            intent.putExtras(bundle)
            if(option.equals("juso")){
                startActivityForResult(intent, Define.REQUEST_SEARCH_JUSO)
                overridePendingTransition(0,0)
            }else{
                startActivity(intent)
                overridePendingTransition(0,0)
            }

        }

        @JavascriptInterface
        fun closeWebView() {
            Dlog.e("closeWebView")
            finish()
        }

        @JavascriptInterface // Med, Reg,
        fun closeWebViewAfterAction(idtype: String, datakey :String, usernm: String, type: String){
            Dlog.e("closeWebViewAfterAction")
            //이전 페이지로 이동
            var resultIntent = Intent()
            resultIntent.putExtra("idtype",idtype)
            resultIntent.putExtra("datakey",datakey)
            resultIntent.putExtra("usernm",usernm)
            resultIntent.putExtra("type",type)
            Dlog.e("idtype : $idtype")
            Dlog.e("datakey : $datakey")
            Dlog.e("usernm : $usernm")
            Dlog.e("type : $type")
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        }

        @JavascriptInterface
        fun openReport(title: String, url: String, option: String, userInfo: String) {
            Dlog.e("opeonReport / $title / $url / $option")
            val intent = Intent(baseContext, EFormViewerActivity::class.java)
            var bundle: Bundle = Bundle().apply {
                putString("title", title)
                putString("url", url)
                putString("option", option)
                putString("userInfo", userInfo)
            }
            intent.putExtras(bundle)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        @JavascriptInterface
        fun sendSuccFlag(flag:String){
            webPageLoadSuccessFlag = flag
            Dlog.e("webPageLoadSuccessFlag $flag 로 세팅됨 ")
        }

        @JavascriptInterface
        fun goToLogin(){
            Dlog.e("goToLogin")
            moveLogin()
        }

        @JavascriptInterface
        fun jusoCallback(roadFullAddr:String,roadAddrPart1:String,addrDetail:String,roadAddrPart2:String,engAddr:String, jibunAddr:String, zipNo:String, admCd:String, rnMgtSn:String, bdMgtSn:String,detBdNmList:String,bdNm:String,bdKdcd:String,siNm:String,sggNm:String,emdNm:String,liNm:String,rn:String,udrtYn:String,buldMnnm:String,buldSlno:String,mtYn:String,lnbrMnnm:String,lnbrSlno:String,emdNo:String){
            Dlog.e("jusoCallback $roadFullAddr,$roadAddrPart1,$addrDetail,$roadAddrPart2,$engAddr, $jibunAddr, $zipNo, $admCd, $rnMgtSn, $bdMgtSn,$detBdNmList,$bdNm,$bdKdcd,$siNm,$sggNm,$emdNm,$liNm,$rn,$udrtYn,$buldMnnm,$buldSlno,$mtYn,$lnbrMnnm,$lnbrSlno,$emdNo")

            //이전 페이지로 이동
            var resultIntent = Intent()
            resultIntent.putExtra("roadFullAddr",roadFullAddr)
            resultIntent.putExtra("roadAddrPart1",roadAddrPart1)
            resultIntent.putExtra("addrDetail",addrDetail)
            resultIntent.putExtra("roadAddrPart2",roadAddrPart2)
            resultIntent.putExtra("engAddr",engAddr)
            resultIntent.putExtra("jibunAddr",jibunAddr)
            resultIntent.putExtra("zipNo",zipNo)
            resultIntent.putExtra("admCd",admCd)
            resultIntent.putExtra("rnMgtSn",rnMgtSn)
            resultIntent.putExtra("bdMgtSn",bdMgtSn)
            resultIntent.putExtra("detBdNmList",detBdNmList)
            resultIntent.putExtra("bdNm",bdNm)
            resultIntent.putExtra("bdKdcd",bdKdcd)
            resultIntent.putExtra("siNm",siNm)
            resultIntent.putExtra("sggNm",sggNm)
            resultIntent.putExtra("emdNm",emdNm)
            resultIntent.putExtra("liNm",liNm)
            resultIntent.putExtra("rn",rn)
            resultIntent.putExtra("udrtYn",udrtYn)
            resultIntent.putExtra("buldMnnm",buldMnnm)
            resultIntent.putExtra("buldSlno",buldSlno)
            resultIntent.putExtra("mtYn",mtYn)
            resultIntent.putExtra("lnbrMnnm",lnbrMnnm)
            resultIntent.putExtra("lnbrSlno",lnbrSlno)
            resultIntent.putExtra("emdNo",emdNo)
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        }
    }

    private fun moveLogin(){
        val intent = Intent(this, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        startActivity(intent)
        overridePendingTransition(0,0)
    }

    //원래 fileuploadid 넘겼다가 받아서 처리?
    fun enterRecordVoiceActivity(fileuploadId: String, filegrpno: String){
        val intent = Intent(baseContext, AudioMediaActivity::class.java)
        var bundle: Bundle = Bundle().apply {
            putString("filegrpno", filegrpno)
        }
        intent.putExtras(bundle)
        startActivityForResult(intent, Define.REQUEST_RECORD_SOUND_ACTION)
        overridePendingTransition(0, 0)
    }

    fun enterRecordVideoActivity(fileuploadId: String, filegrpno: String){
        val intent = Intent(baseContext, CameraMediaActivity::class.java)
        var bundle: Bundle = Bundle().apply {
            putString("filegrpno", filegrpno)
        }
        intent.putExtras(bundle)
        startActivityForResult(intent, Define.REQUEST_RECORD_VIDEO_CAPTURE)
        overridePendingTransition(0, 0)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        var preferenceUtil: PreferenceUtil = PreferenceUtil(baseContext)

        Dlog.e("requestCode : $requestCode")
        Dlog.e("permissions : " + permissions[0])
        Dlog.e("grantResults : " + grantResults[0])

        when (requestCode) {
            Define.REQUEST_RECORD_VOICE_PERMISSION -> {
                Dlog.e("onRequestVoicePermissionsResult in PopUpActivity")
                enterRecordVoiceActivity(id!!, filegroupno!!)
            }

            Define.REQUEST_RECORD_VIDEO_PERMISSION -> {
                Dlog.e("onRequestVideoPermissionsResult in PopUpActivity")
                enterRecordVideoActivity(id!!, filegroupno!!)
            }
            Define.REQUEST_FILE_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    doDownload(downloadUrl, downloadUserAgent, downloadContentDisposition, downloadMimeType)
                } else {
                    finish()
                    Toast.makeText(this@PopUpActivity, "'허용' 해주셔야 다운로드가 가능합니다.", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.e("onActivityResult","onActivityResult $requestCode, $resultCode")
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == RESULT_OK){
            Dlog.e("resultCode = $resultCode")
            Dlog.e("requestCode = $requestCode")
            Dlog.e("data = ${data.toString()}")
            var fileInfo = data!!.getStringExtra("result")

            when(requestCode){
                Define.REQUEST_RECORD_VIDEO_CAPTURE ->{
                    Dlog.e("REQUEST_RECORD_VIDEO_CAPTURE")
                    Dlog.e("video data = $fileInfo")
                    webview_pop_up.loadUrl("javascript:JsUtilNibp.reqRecVodCallback('"+id!!+"','"+fileInfo+"')")
                    Toast.makeText(this,"파일이 업로드 되었습니다.",Toast.LENGTH_LONG).show()
                }
                Define.REQUEST_RECORD_SOUND_ACTION ->{
                    Dlog.e("REQUEST_RECORD_SOUND_ACTION")
                    Dlog.e("voice data = ${data!!.getStringExtra("result")}")
                    webview_pop_up.loadUrl("javascript:JsUtilNibp.reqRecVoiceCallback('"+id!!+"','"+fileInfo+"')")
                    Toast.makeText(this,"파일이 업로드 되었습니다.",Toast.LENGTH_LONG).show()
                }
                REQUEST_SELECT_FILE_LEGACY -> {
                    Dlog.e("REQUEST_SELECT_FILE_LEGACY")
                    if (mUploadMessage == null)
                        return
                    val result = if ((data == null || resultCode !== RESULT_OK)) null else data.getData()
                    mUploadMessage!!.onReceiveValue(result)
                    mUploadMessage = null
                }

                REQUEST_SELECT_FILE -> {
                    Dlog.e("REQUEST_SELECT_FILE")
                    if (mUploadMessageArr == null)
                        return
                    mUploadMessageArr!!.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, data))
                    mUploadMessageArr = null
                }

                RESULT_CANCELED->{
                    Dlog.e("취소")
                }

                REQUEST_SEARCH_JUSO->{
                    webview_pop_up.loadUrl("javascript:jusoCallBack('" +data.getStringExtra("roadFullAddr")+"','"
                                                                            +data.getStringExtra("roadAddrPart1")+"','"
                                                                            +data.getStringExtra("addrDetail")+"','"
                                                                            +data.getStringExtra("roadAddrPart2")+"','"
                                                                            +data.getStringExtra("engAddr")+"','"
                                                                            +data.getStringExtra("jibunAddr")+"','"
                                                                            +data.getStringExtra("zipNo")+"','"
                                                                            +data.getStringExtra("admCd")+"','"
                                                                            +data.getStringExtra("rnMgtSn")+"','"
                                                                            +data.getStringExtra("bdMgtSn")+"','"
                                                                            +data.getStringExtra("detBdNmList")+"','"
                                                                            +data.getStringExtra("bdNm")+"','"
                                                                            +data.getStringExtra("bdKdcd")+"','"
                                                                            +data.getStringExtra("siNm")+"','"
                                                                            +data.getStringExtra("sggNm")+"','"
                                                                            +data.getStringExtra("emdNm")+"','"
                                                                            +data.getStringExtra("liNm")+"','"
                                                                            +data.getStringExtra("rn")+"','"
                                                                            +data.getStringExtra("udrtYn")+"','"
                                                                            +data.getStringExtra("buldMnnm")+"','"
                                                                            +data.getStringExtra("buldSlno")+"','"
                                                                            +data.getStringExtra("mtYn")+"','"
                                                                            +data.getStringExtra("lnbrMnnm")+"','"
                                                                            +data.getStringExtra("lnbrSlno")+"','"
                                                                            +data.getStringExtra("emdNo")+"')")
                }
            }
        }else{
            if (mUploadMessageArr != null) {
                mUploadMessageArr!!.onReceiveValue(null);
                mUploadMessageArr = null;
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (this.dialog != null) {
            dialog!!.dismiss()
            this.dialog = null
        }
    }

    override fun onStop() {
        super.onStop()
        if (this.dialog != null) {
            dialog!!.dismiss()
            dialog = null
        }
    }

    var mUploadMessage: ValueCallback<Uri>? = null
    var mUploadMessageArr: ValueCallback<Array<Uri>>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pop_up)

        toolbar_close_button.setOnClickListener {
            //webpage가 잘 로딩되면 닫으면서 아래 액티비티로 파라미터 넘기기위해 closeFormView 호출
            if("Y".equals(webPageLoadSuccessFlag)){
                MaterialDialog(this)
                        .title(R.string.do_back_title)
                        .message(R.string.do_back_message)
                        .positiveButton (R.string.yes){
                            Dlog.e("뒤로가기 - 예")
                            webview_pop_up.loadUrl("javascript:closeFormView()")
                        }
                        .negativeButton(R.string.no){
                            Dlog.e("뒤로가기 - 아니오")
                        }
                        .show()
            }else{
                finish()
            }
        }

        toolbar_refresh_button.setOnClickListener {
            webview_pop_up.reload()
        }

        var title = intent?.extras?.getString("title")
        toolbar_title.text = title
        url = intent?.extras?.getString("url")!!
        option = intent?.extras?.getString("option")!!

        Dlog.e("받아옴 $title, $url, $option")

        val settings = webview_pop_up!!.settings
        settings.javaScriptEnabled = true
        settings.domStorageEnabled = true
        settings.databaseEnabled = true
        settings.useWideViewPort =  true
        settings.loadWithOverviewMode = true
        settings.setSupportZoom(true)
        settings.builtInZoomControls = true
        settings.displayZoomControls = false
        settings.setSupportMultipleWindows(true)
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.NORMAL

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            settings.safeBrowsingEnabled = false
        }

        webview_pop_up!!.addJavascriptInterface(BitInterface(), "App")

        webview_pop_up!!.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                if(! this@PopUpActivity.isFinishing){
                    waveDialog?.show()
                }

            }
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                if(! this@PopUpActivity.isFinishing){
                    waveDialog?.dismiss()
                }
            }

            override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
                //                handler.proceed(); // SSL 에러가 발생해도 계속 진행!
                val builder = AlertDialog.Builder(this@PopUpActivity)
//                builder.setMessage(R.string.notification_error_ssl_cert_invalid)
                builder.setPositiveButton("continue", DialogInterface.OnClickListener { dialog, which -> handler.proceed() })
                builder.setNegativeButton("cancel", DialogInterface.OnClickListener { dialog, which -> handler.cancel() })
                dialog = builder.create()
                dialog?.show()
            }

            override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
                super.onReceivedError(view, request, error)
                Dlog.e("onReceivedError " + error)
                waveDialog?.dismiss()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    when (error.errorCode) {
                        WebViewClient.ERROR_AUTHENTICATION                      // 서버에서 사용자 인증 실패
                            , WebViewClient.ERROR_BAD_URL                             // 잘못된 URL
                            , WebViewClient.ERROR_CONNECT                             // 서버로 연결 실패
                            , WebViewClient.ERROR_FAILED_SSL_HANDSHAKE                // SSL handshake 수행 실패
                            , WebViewClient.ERROR_FILE                                // 일반 파일 오류
                            , WebViewClient.ERROR_FILE_NOT_FOUND                      // 파일을 찾을 수 없습니다
                            , WebViewClient.ERROR_HOST_LOOKUP                         // 서버 또는 프록시 호스트 이름 조회 실패
                            , WebViewClient.ERROR_IO                                  // 서버에서 읽거나 서버로 쓰기 실패
                            , WebViewClient.ERROR_PROXY_AUTHENTICATION                // 프록시에서 사용자 인증 실패
                            , WebViewClient.ERROR_REDIRECT_LOOP                       // 너무 많은 리디렉션
                            , WebViewClient.ERROR_TIMEOUT                             // 연결 시간 초과
                            , WebViewClient.ERROR_TOO_MANY_REQUESTS                   // 페이지 로드중 너무 많은 요청 발생
                            , WebViewClient.ERROR_UNKNOWN                             // 일반 오류
                            , WebViewClient.ERROR_UNSUPPORTED_AUTH_SCHEME             // 지원되지 않는 인증 체계
                            , WebViewClient.ERROR_UNSUPPORTED_SCHEME -> {

    //                        val builder = AlertDialog.Builder(this@RecordActivity)
    //                        //      builder.setTitle(R.string.ALRET_DIALOG_TITLE);
    //                        builder.setPositiveButton("확인", DialogInterface.OnClickListener { dialog, which -> finish() })
    //                        builder.setMessage("네트워크 상태가 원활하지 않습니다. 잠시 후 다시 시도해 주세요.")
    //                        builder.show()
                        }
                    }
                }else{ //롤리팝 아래

                }

            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                Dlog.e("shouldOverrideUrlLoading(view: WebView, url: String)"+url)
//                view.loadUrl(url)
                return false
            }

            @TargetApi(Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                Dlog.e("shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest)")
                return false//응용프로그램이 직접 url를 처리함
            }

            private fun handleUri(uri: Uri): Boolean {
                val host = uri.getHost()
                val scheme = uri.getScheme()
                // Based on some condition you need to determine if you are going to load the url
                // in your web view itself or in a browser.
                // You can use `host` or `scheme` or any part of the `uri` to decide.

                if (true) {
                    // Returning false means that you are going to load this url in the webView itself
                    return false
                } else {
                    // Returning true means that you need to handle what to do with the url
                    // e.g. open web page in a Browser


                    val intent = Intent(Intent.ACTION_VIEW, uri)
                    startActivity(intent)
                    return true
                }
            }
        }

        webview_pop_up!!.webChromeClient = object : WebChromeClient() {
//            override fun onJsAlert(view: WebView, url: String, message: String, result: JsResult): Boolean {
//                return super.onJsAlert(view, url, message, result)
//            }

            // For Android < 3.0
            fun openFileChooser(uploadMsg: ValueCallback<Uri>) {
                mUploadMessage = uploadMsg
                val i = Intent(Intent.ACTION_GET_CONTENT)
                i.addCategory(Intent.CATEGORY_OPENABLE)
                i.setType("*/*")
                startActivityForResult(Intent.createChooser(i, "파일을 선택해 주세요"), Define.REQUEST_SELECT_FILE_LEGACY)
            }

            // For Android 3.0+
            fun openFileChooser(uploadMsg: ValueCallback<Uri>, acceptType: String) {
                mUploadMessage = uploadMsg
                val i = Intent(Intent.ACTION_GET_CONTENT)
                i.addCategory(Intent.CATEGORY_OPENABLE)
                i.setType(acceptType)
                startActivityForResult(Intent.createChooser(i, "파일을 선택해 주세요"), Define.REQUEST_SELECT_FILE_LEGACY)
            }

            // For Android 4.1+
            fun openFileChooser(uploadMsg: ValueCallback<Uri>, acceptType: String, capture: String) {
                mUploadMessage = uploadMsg
                val i = Intent(Intent.ACTION_GET_CONTENT)
                i.addCategory(Intent.CATEGORY_OPENABLE)
                i.setType(acceptType)
                startActivityForResult(Intent.createChooser(i, "파일을 선택해 주세요"), Define.REQUEST_SELECT_FILE_LEGACY)
            }


            // For Android 5.0+
            @SuppressLint("NewApi")
            override fun onShowFileChooser(webView: WebView, filePathCallback: ValueCallback<Array<Uri>>, fileChooserParams: FileChooserParams): Boolean {
                Dlog.e("For Android 5.0+ onShowFileChooser")
                if (mUploadMessageArr != null) {
                    mUploadMessageArr!!.onReceiveValue(null)
                    mUploadMessageArr = null
                }
                mUploadMessageArr = filePathCallback
                val intent = fileChooserParams.createIntent()
                try {
                    startActivityForResult(intent, Define.REQUEST_SELECT_FILE)
                } catch (e: Throwable) {
                    mUploadMessageArr = null
                    Dlog.e("에러 - 파일선택")
                    return false
                }
                //아래는 하위버전
//                val i = Intent(Intent.ACTION_GET_CONTENT)
//                i.addCategory(Intent.CATEGORY_OPENABLE)
//                i.setType("*/*")
//                startActivityForResult(Intent.createChooser(i, "파일을 선택해 주세요"), Define.REQUEST_SELECT_FILE)
                return true
            }
        }

        webview_pop_up!!.setDownloadListener { url, userAgent, contentDisposition, mimeType, contentLength ->
            downloadUrl = url
            downloadUserAgent = userAgent
            downloadContentDisposition = contentDisposition
            downloadMimeType = mimeType
            doDownload(url, userAgent, contentDisposition, mimeType)
        }

        webview_pop_up!!.clearCache(true)
        webview_pop_up!!.clearHistory()
        Dlog.e("loadUrl $url")

        webview_pop_up!!.setInitialScale(1)

        if(title!!.startsWith("공지사항")){
            Dlog.e("공지사항")
            webview_pop_up!!.loadUrl(url+PreferenceUtil(this@PopUpActivity).getStringExtra("token"))
            Dlog.e("webview_pop_up!!.loadUrl($url${PreferenceUtil(this@PopUpActivity).getStringExtra("token")})")
        }else if(title!!.startsWith("연명의료 시술 설명")){
            Dlog.e("연명의료 시술 설명")
            webview_pop_up!!.loadUrl(url)
            Dlog.e("webview_pop_up!!.loadUrl($url)")
        }else{
            webview_pop_up!!.loadUrl(Define.serverIP+url)
            Dlog.e("webview_pop_up!!.loadUrl(${Define.serverIP}$url)")
        }
    }

    private fun doDownload(url: String, userAgent: String, contentDisposition: String, mimeType: String){
        try{
            val request = DownloadManager.Request(Uri.parse(url))
            request.setMimeType(mimeType)
            //------------------------COOKIE!!------------------------
            val cookies = CookieManager.getInstance().getCookie(url)
            request.addRequestHeader("cookie", cookies)
            //------------------------COOKIE!!------------------------
            request.addRequestHeader("User-Agent", userAgent)
            request.setDescription("Downloading file...")
            request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType))
            request.allowScanningByMediaScanner()
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url, contentDisposition, mimeType))
            val dm = this!!.getSystemService(DOWNLOAD_SERVICE) as DownloadManager
            dm.enqueue(request)
            Toast.makeText(this, "파일을 다운로드합니다.", Toast.LENGTH_LONG).show()
        }catch (e:Exception){
            Toast.makeText(this, "파일 다운로드에 실패했습니다.", Toast.LENGTH_LONG).show()
        }


    }

    override// Detect when the back button is pressed
    fun onBackPressed() {
//        if (webview_pop_up!!.canGoBack()) {
//            webview_pop_up!!.goBack()
//        } else {
//            super.onBackPressed()
//        }
        //공지사항은 다르게 분기해야될 듯
        if("Y".equals(webPageLoadSuccessFlag)){
            MaterialDialog(this)
                    .title(R.string.do_back_title)
                    .message(R.string.do_back_message)
                    .positiveButton (R.string.yes){
                        Dlog.e("뒤로가기 - 예")
                        webview_pop_up.loadUrl("javascript:closeFormView()")
                    }
                    .negativeButton(R.string.no){
                        Dlog.e("뒤로가기 - 아니오")
                    }
                    .show()
        }else{
            finish()
        }
    }

//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    @RequiresApi(api = Build.VERSION_CODES.N)
    fun sendFile(file: File){
        Dlog.e("sendFile")
        Dlog.e("file : "+file.absolutePath)

        val uploadProgressListener = object: UploadProgressListener {
            override fun onProgress(bytesUploaded: Long, totalBytes: Long) {
                Log.e("send file",bytesUploaded.toString()+"%")
                var per:Long = bytesUploaded*100/totalBytes
                Log.e("send file per",per.toString()+"%")
//                upload_text_record.setText( per.toString()+"%")
//                pb_record.setProgress(per.toInt(),true)
           }
        }
//        val jSONObjectRequestListener = object:JSONObjectRequestListener{
//            override fun onResponse(response:JSONObject) {
//                Log.e("send file",response.toString())
//            }

        val stringRequestListener = object: StringRequestListener {
            override fun onResponse(response:String) {
                Log.e("send file",response.toString())
                webview_pop_up!!.loadUrl("javascript:setMessage('" + response.toString()+" 파일 업로드 됨" + "')")
            }
        override fun onError(error: ANError) {
            if (error.errorCode !== 0) {
                // received error from server
                // error.getErrorCode() - the error code from server
                // error.getErrorBody() - the error body from server
                // error.getErrorDetail() - just an error detail
                Log.d("send file", "onError errorCode : " + error.errorCode)
                Log.d("send file", "onError errorBody : " + error.errorBody)
                Log.d("send file", "onError errorDetail : " + error.errorDetail)
                // get parsed error object (If ApiError is your class)
//                    val apiError = error.getErrorAsObject(ApiError::class.java)
            } else {
                // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                Log.d("send file", "onError errorDetail : " + error.errorDetail)
            }
        }
    }

    AndroidNetworking.upload(Define.serverIP+"/bitcare/upload")
             .addMultipartFile("file1",file)
             .addMultipartParameter("email","acid@co.kr")
             .setTag("uploadTest")
             .setPriority(Priority.HIGH)
             .build()
             .setUploadProgressListener(uploadProgressListener)
             .getAsString(stringRequestListener)
//                 .getAsJSONObject(jSONObjectRequestListener)
    }

    /** Create a file Uri for saving an image or video  */
    fun getOutputMediaFileUri(type: Int): Uri {
        return FileProvider.getUriForFile(this, "com.hanbae.acid.testkotlin.fileprovider", getOutputMediaFile(type)!!)
    }

    /** Create a File for saving an image or video  */
    fun getOutputMediaFile(type: Int): File? {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        val mediaStorageDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Upload")
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("getOutputMediaFile", "failed to create directory")
                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(Date())
        val mediaFile: File
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = File(mediaStorageDir.path + File.separator +
                    "IMG_" + timeStamp + ".png")
            Log.d("getOutputMediaFile", "voice : ")
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = File(mediaStorageDir.path + File.separator +
                    "VID_" + timeStamp + ".mp4")
            Log.d("getOutputMediaFile", "video : ")
        } else {
            Log.d("getOutputMediaFile", "return null")
            return null
        }

        return mediaFile
    }

}

