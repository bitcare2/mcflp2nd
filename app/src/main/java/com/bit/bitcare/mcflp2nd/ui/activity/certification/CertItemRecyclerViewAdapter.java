package com.bit.bitcare.mcflp2nd.ui.activity.certification;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bit.bitcare.mcflp2nd.R;
import com.yettiesoft.koreamint.certificate.ManageCertificate;
import com.yettiesoft.koreamint.certificate.pkcs.CertInfo;

import java.util.Vector;

/**
 * Created by pol808 on 2016-08-26.
 */
public class CertItemRecyclerViewAdapter extends RecyclerView.Adapter<CertViewHolder> {

    private Context context;
    ManageCertificate manager;
    CertInfo selectedCertInfo;

    Vector<CertViewHolder> certsViewHolder = new Vector<CertViewHolder>();

    // 인증서 리스트 관련 UI
    public CertItemRecyclerViewAdapter(Context context) {
        this.context = context;
        manager = new ManageCertificate(context);
        manager.loadCertificate();
        if (manager.sizeCertificate() > 0)
            selectedCertInfo = manager.getCertificate(0);
    }

    @Override
    public CertViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_content, parent, false);

        CertViewHolder certView = new CertViewHolder(view);
        if (certsViewHolder.size() == 0) {
            certView.setVisible();
        }
        certsViewHolder.add(certView);

        return certView;
    }

    @Override
    public void onBindViewHolder(final CertViewHolder holder, int position) {
        holder.mItem = manager.getCertificate(position);

        holder.mSubject.setText(holder.mItem.getSujectName());
        holder.mIssue.setText(this.context.getResources().getString(R.string.cert_item_issue) + " " + holder.mItem.getIssueName());
        holder.mValidTo.setText(this.context.getResources().getString(R.string.cert_item_expire) + " " + holder.mItem.getValidTo());

        int imageRes = R.drawable.ic_certificate_true;

        switch (holder.mItem.getExpire()) {
            case 0: // 정상 인증서
                imageRes = R.drawable.ic_certificate_true;
                break;
            case 1: // 만료 한달전 인증서
                imageRes = R.drawable.ic_certificate_re;
                break;
            case -1: // 만료 인증서
                imageRes = R.drawable.ic_certificate_false;
                break;
        }

        holder.mExpire.setImageResource(imageRes);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //////////////////////////////////////////////////////////////
                // 선택 인증서
                selectedCertInfo = holder.mItem;

                for (CertViewHolder cert: certsViewHolder) {
                    cert.setUnVisible();
                }

                ImageView imageView = (ImageView)holder.mView.findViewById(R.id.certConfirm);
                imageView.setVisibility(View.VISIBLE);

            }
        });
    }

    @Override
    public int getItemCount() {
        return manager.sizeCertificate();
    }
    public CertInfo getSelectedSubjectName() { return this.selectedCertInfo; }
}

