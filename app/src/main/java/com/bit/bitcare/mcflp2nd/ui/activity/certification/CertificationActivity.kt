package com.bit.bitcare.mcflp2nd.ui.activity.certification

import android.Manifest
import android.annotation.TargetApi
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.webkit.*
import android.widget.Toast

import com.afollestad.materialdialogs.MaterialDialog
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.data.model.ResponseMyInfoVo
import com.bit.bitcare.mcflp2nd.data.model.ResponseVo
import com.bit.bitcare.mcflp2nd.ui.activity.BaseActivity
import com.bit.bitcare.mcflp2nd.ui.activity.IdPwLoginActivity
import com.bit.bitcare.mcflp2nd.ui.activity.MainActivity
import com.bit.bitcare.mcflp2nd.ui.activity.PopUpActivity
import com.bit.bitcare.mcflp2nd.ui.activity.media.AudioMediaActivity
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.Define.Companion.mobileServerIP
import com.bit.bitcare.mcflp2nd.util.Dlog
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil
import com.bit.bitcare.mcflp2nd.util.Util
import com.yettiesoft.koreamint.certificate.ExtractCertificate
import org.apache.commons.lang3.ArrayUtils
import org.apache.commons.lang3.StringUtils
import org.json.JSONObject

import java.io.UnsupportedEncodingException
import java.lang.Exception
import java.net.URLEncoder

class CertificationActivity : BaseActivity() {

    private val REQUEST_READWRITE_STORAGE = 0

    private var webView: WebView? = null
    private var dnValue: String? = null

    companion object {
        val REG = "Reg"
        val MED = "Med"

        private val PKIORGMED = "01"
        private val PKIBIZ = "02"
        private val PKIPERSON = "03"

        //    private static final String url = "https://122.199.232.74:8443/bitcare/resources/mobile/mobilecert.html"; //로컬
        //    private static final String url = "https://intra.lst.go.g2e:8484/mobile/mobilecert.html"; //개발
        //    private static final String url = "https://intra.lst.go.g2e:8484/vestsign/sample/mobileTest.html"; //개발 샘플페이지
        //    private static final String url = "https://intra.lst.go.kr/mobile/mobilecert.html"; //운영
//        private val url = "https://nibp-intra.g2e.co.kr/rm/mobile/mobilecert.do" //
    }

    private val pkiPolicyIdentifierForPerson: Array<String>
        get() = arrayOf("1.2.410.200004.5.2.1.2", "1.2.410.200004.5.1.1.5", "1.2.410.200005.1.1.1", "1.2.410.200004.5.4.1.1", "1.2.410.200012.1.1.1", "1.2.410.200004.5.4.1.101", "1.2.410.200004.5.4.1.102", "1.2.410.200004.5.4.1.104", "1.2.410.200004.5.2.1.7.1", "1.2.410.200004.5.2.1.7.2", "1.2.410.200004.5.2.1.7.3", "1.2.410.200004.5.1.1.9", "1.2.410.200004.5.1.1.9.2", "1.2.410.200005.1.1.4", "1.2.410.200005.1.1.6.2", "1.2.410.200012.1.1.101", "1.2.410.200012.1.1.103", "1.2.410.200012.1.1.105")

    private val pkiPolicyIdentifierForBiz: Array<String>
        get() = arrayOf("1.2.410.200004.5.1.1.7", "1.2.410.200005.1.1.5", "1.2.410.200004.5.4.1.2", "1.2.410.200004.5.4.1.201", "1.2.410.200012.1.1.3", "1.2.410.200005.1.1.2", "1.2.410.200004.5.2.1.1", "1.2.410.200004.5.2.1.6.141")

    private val pkiPolicyIdentifierForOrgmed: Array<String>
        get() = arrayOf("1.2.410.200004.5.2.1.6.141", "1.2.410.200004.5.2.1.6.142")

    inner class AndroidBridge {
        @JavascriptInterface
        fun message(dn: String, pi: String) {
            if ("close" == dn) {
                Dlog.e("close")
                finish()
            } else {
                Dlog.e("dn : $dn")

                val pkiClsCode = getPkiType(pi)  // 인증서 타입 구분
                Dlog.e("pkiClsCode : $pkiClsCode")

                dnValue = dn
                divideLogin(dn, pkiClsCode)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_certification)
        Dlog.e("onCreate")

        webView = this.findViewById(R.id.webview_certification) as WebView

                webView!!.clearHistory();
                webView!!.clearCache(true);
                webView!!.clearView();

        val settings = webView!!.settings
        settings.javaScriptEnabled = true
        settings.domStorageEnabled = true
        settings.databaseEnabled = true
        settings.useWideViewPort = false
        settings.setSupportMultipleWindows(true)
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.NORMAL

        //////////////////////////////////////////////////////////////
        // 인증서 복사 Class 등록
        // JSInterfaceNoSloth Class 등록
        // yettie 이름 변경 불가
        webView!!.addJavascriptInterface(JSInterfaceNoSloth(this), "yettie")
        webView!!.addJavascriptInterface(AndroidBridge(), "verificationResult")
        webView!!.webViewClient = object : android.webkit.WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
//                super.onPageFinished(view, url)
                Dlog.e("onPageFinished")
                Dlog.e("url: " + url)
                if("about:blank".equals(url)) {
                    Dlog.e("o-o: https://intra.lst.go.kr/rm/mobile/mobilecert.do")
                    webView!!.loadUrl("https://intra.lst.go.kr/rm/mobile/mobilecert.do")
                }
            }

            override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
                //                handler.proceed(); // SSL 에러가 발생해도 계속 진행!
                val builder = AlertDialog.Builder(this@CertificationActivity)
                builder.setMessage(R.string.notification_error_ssl_cert_invalid)
                builder.setPositiveButton("continue") { dialog, which -> handler.proceed() }
                builder.setNegativeButton("cancel") { dialog, which -> handler.cancel() }
                val dialog = builder.create()
                dialog.show()
            }

            @TargetApi(Build.VERSION_CODES.M)
            override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
                super.onReceivedError(view, request, error)
                Dlog.e("onReceivedError")
                when (error.errorCode) {
                    WebViewClient.ERROR_AUTHENTICATION                      // 서버에서 사용자 인증 실패
                        , WebViewClient.ERROR_BAD_URL                             // 잘못된 URL
                        , WebViewClient.ERROR_CONNECT                             // 서버로 연결 실패
                        , WebViewClient.ERROR_FAILED_SSL_HANDSHAKE                // SSL handshake 수행 실패
                        , WebViewClient.ERROR_FILE                                // 일반 파일 오류
                        , WebViewClient.ERROR_FILE_NOT_FOUND                      // 파일을 찾을 수 없습니다
                        , WebViewClient.ERROR_HOST_LOOKUP                         // 서버 또는 프록시 호스트 이름 조회 실패
                        , WebViewClient.ERROR_IO                                  // 서버에서 읽거나 서버로 쓰기 실패
                        , WebViewClient.ERROR_PROXY_AUTHENTICATION                // 프록시에서 사용자 인증 실패
                        , WebViewClient.ERROR_REDIRECT_LOOP                       // 너무 많은 리디렉션
                        , WebViewClient.ERROR_TIMEOUT                             // 연결 시간 초과
                        , WebViewClient.ERROR_TOO_MANY_REQUESTS                   // 페이지 로드중 너무 많은 요청 발생
                        , WebViewClient.ERROR_UNKNOWN                             // 일반 오류
                        , WebViewClient.ERROR_UNSUPPORTED_AUTH_SCHEME             // 지원되지 않는 인증 체계
                        , WebViewClient.ERROR_UNSUPPORTED_SCHEME -> {

                        val builder = AlertDialog.Builder(this@CertificationActivity)
                        //      builder.setTitle(R.string.ALRET_DIALOG_TITLE);
                        builder.setPositiveButton("확인") { dialog, which -> finish() }
                        builder.setMessage("네트워크 상태가 원활하지 않습니다. 잠시 후 다시 시도해 주세요.")
                        builder.show()
                    }
                }// URI가 지원되지 않는 방식

            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                val uri = Uri.parse(url)
                //                return handleUri(uri);
                view.loadUrl(url)
                return true//응용프로그램이 직접 url를 처리함
            }

            @TargetApi(Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                Dlog.e("Uri =${request.url}")
                val uri = request.url
                //                return handleUri(uri);
                view.loadUrl(uri.path)
                return true//응용프로그램이 직접 url를 처리함
            }

            private fun handleUri(uri: Uri): Boolean {
                Dlog.e("Uri =$uri")
                val host = uri.host
                val scheme = uri.scheme
                // Based on some condition you need to determine if you are going to load the url
                // in your web view itself or in a browser.
                // You can use `host` or `scheme` or any part of the `uri` to decide.

                if (true) {
                    // Returning false means that you are going to load this url in the webView itself
                    return false
                } else {
                    // Returning true means that you need to handle what to do with the url
                    // e.g. open web page in a Browser
                    val intent = Intent(Intent.ACTION_VIEW, uri)
                    startActivity(intent)
                    return true
                }
            }

        }
        webView!!.webChromeClient = SSLWebChromeClient(this)
        //webView.clearCache(true);
        // webView.clearHistory();
        Dlog.e("1111: " + Define.certLoginUrl)
        webView!!.loadUrl(Define.certLoginUrl)
        Dlog.e("2222")

        val permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_READWRITE_STORAGE)
        }
    }

    fun viewError(message: String) {
        MaterialDialog(this)
                .title(R.string.fail_login)
                .message(text = message)
                .positiveButton (R.string.done){
                    finish()
                }
                .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (data != null && data.extras != null) {
            val js = data.extras!!.getString("js")
            if (js != null) {
                //////////////////////////////////////////////////////////////
                // 인증서 내보내기 script 함수 호출
                webView!!.loadUrl(js)
            }
        } else {
            webView!!.loadUrl(ExtractCertificate.cancelMakePFXNoSloth())
        }
    }

    // 로그인 구분
    fun divideLogin(dn: String, pkiClsCode: String) {
        Dlog.e("dn : $dn")
        Dlog.e("pkiClsCode : $pkiClsCode")

        if (pkiClsCode == PKIORGMED) {
            Dlog.e("의료기관용 인증서 01 - 로그인")
            PreferenceUtil(this@CertificationActivity).putStringExtra("LoginType", MED)
            PreferenceUtil(this@CertificationActivity).putStringExtra("dn", dnValue)
            PreferenceUtil(this@CertificationActivity).putStringExtra("pkiClsCode", pkiClsCode)

            var bundle = Bundle()
            bundle.putString("dn", dnValue)
            bundle.putString("pkiClsCode", pkiClsCode)

            val intent = Intent(baseContext, IdPwLoginActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()

        } else {
            Dlog.e("의료기관외 로그인")
            PreferenceUtil(this@CertificationActivity).putStringExtra("LoginType", REG)
            doLogin(dn, pkiClsCode)
        }
    }

    // 상담(등록)기관 로그인
    private fun doLogin(dn: String, pkiClsCode: String) {
        var dn = dn
        var pkiClsCode = pkiClsCode

        try {
            dn = URLEncoder.encode(dn, "utf-8")
            Dlog.e("URLEncoding : $dn")
        } catch (e: UnsupportedEncodingException) {
            Dlog.e("ERROR")
        }

        AndroidNetworking.get(mobileServerIP+"/cert/userCheck")
                .addQueryParameter("prvcertdn", dn)
                .addQueryParameter("pkiClsCode", pkiClsCode)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Dlog.e("받아옴?")
                        Dlog.e(response.toString())

                        var loginVo = gson.fromJson(response.toString(), ResponseVo::class.java)
                        Dlog.e(loginVo.toString())
                        if(loginVo.result.token==null){
                            viewError(loginVo.serviceMsg)
                        }else{
                            PreferenceUtil(this@CertificationActivity).putStringExtra("token",loginVo.result.token)
                            getMyInfo()
                        }
                    }

                    override fun onError(error: ANError) {
                        Toast.makeText(this@CertificationActivity, "통신 에러\n관리자에게 문의해주세요.",Toast.LENGTH_LONG).show()
                    }
                })
    }

    fun getMyInfo() {
        AndroidNetworking.get(mobileServerIP+"/comm/getMyInfoBySn")
                .addQueryParameter("token", PreferenceUtil(this@CertificationActivity).getStringExtra("token"))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        try {
                            Dlog.e("받아옴?")
                            Dlog.e(response.toString())

                            var responseMyInfoVo = gson.fromJson(response.toString(), ResponseMyInfoVo::class.java)
                            Dlog.e(responseMyInfoVo.toString())
                            if ("100".equals(responseMyInfoVo.serviceCode)) {
                                if (responseMyInfoVo.result.token == null) {
                                    viewError(responseMyInfoVo.serviceMsg)
                                } else {
                                    PreferenceUtil(this@CertificationActivity).putStringExtra("token", responseMyInfoVo.result.token)

                                    if (responseMyInfoVo.result.loginInfo != null) {
                                        PreferenceUtil(this@CertificationActivity).putStringExtra("usernm", responseMyInfoVo.result.loginInfo.usernm)

                                        val intent = Intent(this@CertificationActivity, MainActivity::class.java)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                        startActivity(intent)
                                        overridePendingTransition(0, 0)
                                    }
                                }
                            } else {
                                Toast.makeText(this@CertificationActivity, "에러코드 : " + responseMyInfoVo.serviceCode + "\n에러메세지 : " + responseMyInfoVo.serviceMsg, Toast.LENGTH_LONG).show()
                            }
                        }catch (e: Exception){
                            Toast.makeText(this@CertificationActivity, "로그인 에러\n관리자에게 문의해주세요.",Toast.LENGTH_LONG).show()
                        }
                    }
                    override fun onError(error: ANError) {
                        Toast.makeText(this@CertificationActivity, "통신 에러\n관리자에게 문의해주세요.",Toast.LENGTH_LONG).show()
                    }
                })
    }

    /*
     * vidType > PKIORGMED : 의료기관용, PKIBIZ : 법인공인인증서, PKIPERSON : 개인공인인증서
     */

    fun getPkiType(policyIdentifier: String): String {
        var pkiType = ""
        if (ArrayUtils.contains(pkiPolicyIdentifierForOrgmed, policyIdentifier)) {
            // 의료기관용 인증서(건강보험공단 발행 인증서)
            pkiType = PKIORGMED
        } else if (ArrayUtils.contains(pkiPolicyIdentifierForBiz, policyIdentifier)) {
            // 법인공인인증서
            pkiType = PKIBIZ
        } else if (ArrayUtils.contains(pkiPolicyIdentifierForPerson, policyIdentifier)) {
            // 개인인증서(NPKI, GPKI)
            pkiType = PKIPERSON
        }

        if (StringUtils.isEmpty(pkiType)) {
            pkiType = PKIPERSON
        }
        return pkiType
    }


}
