package com.bit.bitcare.mcflp2nd.ui.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.ViewGroup
import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.ui.fragment.JobHistoryFragment
import com.bit.bitcare.mcflp2nd.ui.fragment.TemporarySaveFragment
import com.bit.bitcare.mcflp2nd.ui.fragment.UserSearchFragment
import com.bit.bitcare.mcflp2nd.util.Dlog
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil

class MainPagerAdapter(fm: FragmentManager, val context: Context, val pageCount: Int) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> UserSearchFragment()
            1 -> TemporarySaveFragment()
//            2 -> JobListFragment()
            else -> return JobHistoryFragment()
        }
    }

    override fun getCount(): Int {
        return pageCount
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> {
                context.getString(R.string.title_search)
            }
            1 -> {
                context.getString(R.string.title_temporary_save)
            }
//            2 -> {
//                context.getString(R.string.title_job_list)
//            }
            else -> {
                return context.getString(R.string.title_job_history)
            }
        }
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        var preferenceUtil: PreferenceUtil = PreferenceUtil(context)

        val obj = super.instantiateItem(container, position)
        if (obj is Fragment) {
            val tag = obj.tag
            Dlog.e("in instatniatltem tag : $tag")

            when (position) {
                0 -> {
                    preferenceUtil.putStringExtra("UserSearchFragmentTag", tag)
                }
                1 -> {
                    preferenceUtil.putStringExtra("TemporarySaveFragmentTag", tag)
                }
//                2 -> {
//                    preferenceUtil.putStringExtra("JobListFragmentTag", tag)
//                }
                else -> {
                    preferenceUtil.putStringExtra("JobHistoryFragmentTag", tag)
                }
//                mFragmentTags.put(position, tag)
            }
        }
        return obj
    }
}