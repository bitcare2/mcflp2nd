package com.bit.bitcare.mcflp2nd.data.model

data class MenuVo(
    var title: String?,
    var url: String?,
    var option: String?
)