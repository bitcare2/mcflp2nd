package com.bit.bitcare.mcflp2nd.ui.activity

import android.content.Intent
import android.content.Intent.*
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.Dlog
import kotlinx.android.synthetic.main.activity_e_form_viewer.*
import kotlinx.android.synthetic.main.activity_e_form_viewer.*
import kotlinx.android.synthetic.main.toolbar_close_button.*
import oz.api.OZReportAPI
import oz.api.OZReportCommandListener
import oz.api.OZReportViewer

class EFormViewerActivity : BaseActivity() {
    companion object {
        init {
            System.loadLibrary("skia_android")
            System.loadLibrary("ozrv")
        }
    }

    private var url = ""
    private var viewer: OZReportViewer? = null
    private var option = ""
    private var userInfo = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_e_form_viewer)

        toolbar_close_button.setOnClickListener{
            finish()
        }

        toolbar_title.text = intent?.extras?.getString("title")
        url = intent?.extras?.getString("url")!!
        option = intent?.extras?.getString("option")!!
        userInfo = intent?.extras?.getString("userInfo")!!

        Dlog.e("받아옴 $title, $url, $option")

        var result = checkPermissions(Define.needFilePermissions, Define.REQUEST_FILE_PERMISSION)
        Dlog.e("checkPermissions result : " + result)
        if (result) {
            initOZViewer()
        }
    }

    private fun initOZViewer() {
        Dlog.e("OZViewer started")

        val oZReportCommandListener = object:OZReportCommandListener() {
            override fun OZUserEvent(param1: String, param2: String, param3: String): String {
                return ""
            }
            override fun OZErrorCommand(code: String?, message: String?, detailmessage: String?, reportname: String?) {
                Dlog.e("code : $code")
                Dlog.e("message : $message")
                Dlog.e("detailmessage : $detailmessage")
                Dlog.e("reportname : $reportname")
//                super.OZErrorCommand(code, message, detailmessage, reportname)

                MaterialDialog(this@EFormViewerActivity)
                        .title(R.string.error)
                        .message(null, message)
                        .positiveButton(R.string.close) {
                            Dlog.e("종료 - 예")
                            finish()
                        }
                        .show()
            }
        }

        Dlog.d("OZViewer 오즈 뷰어 패러미터 설정 전 $option")
        Dlog.d("OZViewer 오즈 뷰어 패러미터 설정 전 $userInfo")
        //오즈 뷰어 패러미터 설정

       // option = "{\"explan1\":\"Y\",\"explan2\":\"Y\",\"explan3\":\"Y\",\"explan4\":\"Y\",\"explan5\":\"Y\",\"explan6\":\"Y\",\"item1\":\"Y\",\"item2\":\"Y\",\"lstplantempsn\":\"1785\",\"regcomtyn\":\"N\",\"usersn\":\"22\",\"orgmedno\":\"M18100001\",\"regno\":\"A18-1785\",\"patiusernm\":\"구노석\",\"patiresid\":\"8112251328817\",\"patibday\":\"1981-12-25\",\"patigender\":\"1\",\"patizipno\":\"07036\",\"patiaddr1\":\"서울특별시 동작구 양녕로22라길 50 (상도동)\",\"patiaddr2\":\"1234\",\"bjdcode\":\"1159010200\",\"patitn\":\"010-1234-1234\",\"patistatecode\":\"02\",\"docnm\":\"아구몬\",\"doclicence\":\"7777777\",\"docorgmednm\":\"test2O\",\"hosuseyn\":\"Y\",\"selfyn\":\"Y\",\"confrmethcode\":\"03\",\"confrday\":\"2018-09-03\",\"confsignimgfilegrpno\":\"\",\"repsignimgfilegrpno\":\"\",\"readacccode\":\"01\",\"planday\":\"2018-09-03\",\"docusersn\":\"22\",\"docsignimgfilegrpno\":\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAABkCAIAAABM5OhcAAADG0lEQVR42u3dQW7bMBRF0WQLnXeY/S+ow867BSdAgkKwHeWTfE/8n7xn0FkZkrq2ZMFKXm+32wug9kpYcCAsWBAWLAgLFoQFC8KCBWHBgrBgQViwICxYEBYsCAsWhAULwoIFYcGCsGBBWLAgLFgQFiwICxaEVca/328f//76+2f2REIIqwzCggVhwUIb1udoQR0/lLDKUIXVlNSnBcN63IUq5wI5bVjBcbp/aKKwgq+kPcM6OcDn+za4XVXD+jGm45JqXb1qPV175KUoCatjnGlhdZzjuhe5gLuwjrv33W6otqvv9TwhrLukmma87ZvWceHxYiTblT0syWX45mEdRTZh8bCEn+z2DKv7PV5yNkwalvbC6JqPytlErqh8m5AoLN/Npw3DGqlKsglZwrLe0iSsvv9+/WWWKyzT4dwtrMGqVJswPyz3sdw2rLkfdyaHdcENzK3CGn+7Um1CirCsB3KfsEZuI8s3Yf6p0Kp1eYSl2gTCGt2OJFRVqTZh2bD6rt4WCCvJ5NcMq/salrC081knrAz3Bsut2jelFcKSXGRUDyvPzGuH9fT7kNOvW2dtQqpp1wvr5Mu1Fz81kARhNU8rQr6b5drKOeFcYWV46ibncSo34Sxh5XkYMOdxKjfhFGF1PGh7YtZjcThaMKzW9YzvCCQS3W64M/FBAIzLG9bL1KeXMIiwYEFYsFg/LOE4iCMsWBBWPalu6Hxni7C0Q2XYEx/VFhHW7oTfrz/aJSz5aOvRfmOHsPCFsBINuBjh/uwYlmnMcyVqJqyZY3Z/LsvfFmFNG7bpiQ/TBy4r1Z7vG1bryD8+7md9MOQyhCUYOT74Y1UrXVo9rpSwRgcXKtfQU4SlGXzEGiXdIaxNZ1JlpYRVbCZVVkpYxWZSZaWEVWwmVVaaOqw8CKsVYYUQVivCCiGsVoQVQlitCCuEsFoRVkjOX7TnWylhXYSwWhFWVJLfkp3q76udIKwG09vK9vfVThBWG/fzokH5z8iE1WNWXvl7+o+wYEFYsCAsWBAWLAgLFoQFC8KCBWHBgrBgQViwICxYEBYsCAsW7yh+12ZBUTRqAAAAAElFTkSuQmCC\",\"orignfilegrpno\":\"63227\",\"enterpgm\":\"A01\",\"enterdate\":\"2018-08-31 14:55:13.655945\",\"enterid\":\"22\",\"enternm\":\"아구몬\",\"updatepgm\":\"A01\",\"updatedate\":\"2018-09-04 18:44:40.928808\",\"updateid\":\"22\",\"updatenm\":\"아구몬\",\"rmk\":\"모바일 등록\",\"lstplanstatcode\":\"01\"}"
//        option="{\n" +
//                "  \"bjdcode\": \"123456\",\n" +
//                "  \"confrday\": \"123456\",\n" +
//                "  \"confrmethcode\": \"01\",\n" +
//                "  \"confsignimgfilegrpno\": \"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAcIAAAGQCAYAAAA9XmC5AAAABHNCSVQICAgIfAhkiAAAFEhJREFUeJzt3d11GlnagNHTs+a+5AhQR4A6AuQIhCMARQCKQCgC4QhcisA4AtERCEcAEwFMBMxFf/hzqy2oKkBU8e69ltZc2BZluYeHU+enfluv1+sEAEH969QXAACnJIQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhMBOf/75Z3p4eEgfP35Mv/32W/rw4UO6vb1Nq9Xq1JcGe/ttvV6vT30RQL38+eefaTqd/vh6y/39fRqNRu92XXAMQgiklFL69u1bmkwmaTKZFB7pXVxcpOVyeeQrg+MSQgisSvxe8xZC0/371BcAvK/ZbJaenp5Snud7z/F1Op0DXRWcjhBCAKvVKn379i2Nx+M0m832/n5ZlqVut5vG4/EBrg5OSwjhjC0Wi/Tw8JDyPN/r+2RZlq6vr398XV1dHeYCoQaEEM7QarVKd3d3ewXw5uZG+AhBCOGMrFar9Pnz5zQejyvN//V6vXR9fZ263W66uLg4whVC/QghnIF9Anhzc5O63a74EZYQQsM9PT2l0WiUFotF4T/TarXScDhM3W43XV5eHu3aoAmEEBpqOp2m29vbUgHs9Xqp3++n6+vro10XNI0QQsPMZrN0d3e39eiz13q9XhqNRkZ/8AtCCA1RZSWoAMJuQggN8PDwUGohTKfTSePx2LYHKEAIocYmk0m6u7srPA/YbrfTeDw2BwglCCHU0GKxSLe3t4XnAVutVhqNRqnf7x/1uuAceTAv1MhmHvD3338vFMEsy9L9/X1aLBYiCBUZEUJNPD09peFwWHgesNfrpfF4bBM87EkI4cTKboewEAYOy61ROJHNbdA//vijUARbrVb6+vVrmk6nIggHZEQIJzCZTNLt7W2h26BZlqXhcJhGo9HxLwwCEkJ4R6vVKt3e3qbJZFLo95sHhONzaxTeydPTU/r9998LRbDdbqfn5+eU57kIwpEZEcKRldkTmGVZGo1GaTgcHv/CgJSSESEc1efPnwsvhhkMBmmxWIggvDMjQjiC2WyWbm9v02w22/l7W61WyvPcsWhwIkaEcGAPDw/pjz/+KBTB+/v7NJvNRBBOyIgQDmQ6naa7u7tCAWy32ynPc/sBoQaMCGFPm43xHz9+3BnBzdmgs9lMBKEmjAhhD9PpNN3e3hZ6TFKn00l5nh/8IbmLxSL95z//+bEg5/X/ppTS9fV16vf7qdfrHfS14Rz8tl6v16e+CGiaMhvjj7ElYjqdpqenpzSZTAof0p1SSo+Pj1alwitCCCWVOR7t5uYmjcfjg4wCF4tFenp6SnmeF35Q72tXV1fp5eVl72sparVapW/fvqXpdJpms1mazWbp8vIyXV1dpcfHx4OPjqEKIYSCFotFuru7KzwKzPM8dbvdvV5zE5LxeFxoEU4Rx/6/fNFrvri4SC8vL2LIyVksAwVsNsYXiWCv10uLxaJyBFerVXp6ekqfPn1KHz58SP1+/2AR7HQ6B/k+v7I5QafoNa9WqzQej492PVCUxTKwRZnj0fbZGL8ZRU0mk8IHcldxjPnBxWKRHh4eUp7npf/soQIP+xBCeMPDw0PhRx8NBoM0Go1KH5C9iUjZRS+vdTqddHl5mWazWfr+/fsvf8/Nzc3et2p/tk8ANxwoTi2sgb95eXlZX11drVNKO7/a7fb6+fm59GvM5/N1v98v9BrbXvvLly/r5XL547ovLi7e/P3z+fwgP59DXPvm68uXLwe5JtiHEML/WS6X69FoVPhN/P7+vvRrPD8/7xWRVqu1HgwG/4jacrncGsEq1/raIQO4+btAHQghrP8K1OXlZaE38E6ns355eSn9/a+vrysFI8uyda/Xe3PkuVwut45g2+32Xj+bqgHs9Xrrdrv95q9XGUnDMQghoS2Xy/VwOCwcpMfHx1LfP8/zwrdZfxW/r1+/7rz+bd8/y7LKt0T3CeB8Pl/P5/OtvwfqQggJq+wosGhQlsvlejweF/7erwOxK34/v86uyFYZdS2Xy70CuLFtBHyo+Uo4BCEkpKJzgVmWFV7QMZ/P18PhcOtc3bbRX5k4FIlg2YUomznSstf/q2v/+vWr0SCNIYSEUmZF6M3NzY8VmdtUXQCTZdn6/v6+0Gu8tuv1ysYmz/PSI9i34r1cLt/8XvvcqoVjEULCGI/Hhd7gW61WoduT+8z/VQ3gen3YCFZZxLNr9Lrt+g6xerWuXs+p9vv9U18SBQkhZ28+nxd+sx8MBlsDtbl9WGX+r9Vq7b1vbjAYHCSC8/l83e12DxrA9Xr7LdFz3S6x7ZbyYDA49eVRgBBy1r58+VJozivLsq2jwH32/3U6nYNsHP/y5cveESy7V7JoANfrv+K67Wd9jtslxuPx1r/zxcXFqS+RAoSQs7RcLguPeN6aC9xn9ecmIId6898VwSJ7BfM8L7UQpsxK2fV6+yrRcxsZlZlTpf78\",\n" +
//                "  \"delyn\": \"N\",\n" +
//                "  \"doclicence\": \"123456\",\n" +
//                "  \"docnm\": \"123456\",\n" +
//                "  \"docorgmednm\": \"123456\",\n" +
//                "  \"docsignimgfilegrpno\":\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAcIAAAGQCAYAAAA9XmC5AAAABHNCSVQICAgIfAhkiAAAFEhJREFUeJzt3d11GlnagNHTs+a+5AhQR4A6AuQIhCMARQCKQCgC4QhcisA4AtERCEcAEwFMBMxFf/hzqy2oKkBU8e69ltZc2BZluYeHU+enfluv1+sEAEH969QXAACnJIQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhMBOf/75Z3p4eEgfP35Mv/32W/rw4UO6vb1Nq9Xq1JcGe/ttvV6vT30RQL38+eefaTqd/vh6y/39fRqNRu92XXAMQgiklFL69u1bmkwmaTKZFB7pXVxcpOVyeeQrg+MSQgisSvxe8xZC0/371BcAvK/ZbJaenp5Snud7z/F1Op0DXRWcjhBCAKvVKn379i2Nx+M0m832/n5ZlqVut5vG4/EBrg5OSwjhjC0Wi/Tw8JDyPN/r+2RZlq6vr398XV1dHeYCoQaEEM7QarVKd3d3ewXw5uZG+AhBCOGMrFar9Pnz5zQejyvN//V6vXR9fZ263W66uLg4whVC/QghnIF9Anhzc5O63a74EZYQQsM9PT2l0WiUFotF4T/TarXScDhM3W43XV5eHu3aoAmEEBpqOp2m29vbUgHs9Xqp3++n6+vro10XNI0QQsPMZrN0d3e39eiz13q9XhqNRkZ/8AtCCA1RZSWoAMJuQggN8PDwUGohTKfTSePx2LYHKEAIocYmk0m6u7srPA/YbrfTeDw2BwglCCHU0GKxSLe3t4XnAVutVhqNRqnf7x/1uuAceTAv1MhmHvD3338vFMEsy9L9/X1aLBYiCBUZEUJNPD09peFwWHgesNfrpfF4bBM87EkI4cTKboewEAYOy61ROJHNbdA//vijUARbrVb6+vVrmk6nIggHZEQIJzCZTNLt7W2h26BZlqXhcJhGo9HxLwwCEkJ4R6vVKt3e3qbJZFLo95sHhONzaxTeydPTU/r9998LRbDdbqfn5+eU57kIwpEZEcKRldkTmGVZGo1GaTgcHv/CgJSSESEc1efPnwsvhhkMBmmxWIggvDMjQjiC2WyWbm9v02w22/l7W61WyvPcsWhwIkaEcGAPDw/pjz/+KBTB+/v7NJvNRBBOyIgQDmQ6naa7u7tCAWy32ynPc/sBoQaMCGFPm43xHz9+3BnBzdmgs9lMBKEmjAhhD9PpNN3e3hZ6TFKn00l5nh/8IbmLxSL95z//+bEg5/X/ppTS9fV16vf7qdfrHfS14Rz8tl6v16e+CGiaMhvjj7ElYjqdpqenpzSZTAof0p1SSo+Pj1alwitCCCWVOR7t5uYmjcfjg4wCF4tFenp6SnmeF35Q72tXV1fp5eVl72sparVapW/fvqXpdJpms1mazWbp8vIyXV1dpcfHx4OPjqEKIYSCFotFuru7KzwKzPM8dbvdvV5zE5LxeFxoEU4Rx/6/fNFrvri4SC8vL2LIyVksAwVsNsYXiWCv10uLxaJyBFerVXp6ekqfPn1KHz58SP1+/2AR7HQ6B/k+v7I5QafoNa9WqzQej492PVCUxTKwRZnj0fbZGL8ZRU0mk8IHcldxjPnBxWKRHh4eUp7npf/soQIP+xBCeMPDw0PhRx8NBoM0Go1KH5C9iUjZRS+vdTqddHl5mWazWfr+/fsvf8/Nzc3et2p/tk8ANxwoTi2sgb95eXlZX11drVNKO7/a7fb6+fm59GvM5/N1v98v9BrbXvvLly/r5XL547ovLi7e/P3z+fwgP59DXPvm68uXLwe5JtiHEML/WS6X69FoVPhN/P7+vvRrPD8/7xWRVqu1HgwG/4jacrncGsEq1/raIQO4+btAHQghrP8K1OXlZaE38E6ns355eSn9/a+vrysFI8uyda/Xe3PkuVwut45g2+32Xj+bqgHs9Xrrdrv95q9XGUnDMQghoS2Xy/VwOCwcpMfHx1LfP8/zwrdZfxW/r1+/7rz+bd8/y7LKt0T3CeB8Pl/P5/OtvwfqQggJq+wosGhQlsvlejweF/7erwOxK34/v86uyFYZdS2Xy70CuLFtBHyo+Uo4BCEkpKJzgVmWFV7QMZ/P18PhcOtc3bbRX5k4FIlg2YUomznSstf/q2v/+vWr0SCNIYSEUmZF6M3NzY8VmdtUXQCTZdn6/v6+0Gu8tuv1ysYmz/PSI9i34r1cLt/8XvvcqoVjEULCGI/Hhd7gW61WoduT+8z/VQ3gen3YCFZZxLNr9Lrt+g6xerWuXs+p9vv9U18SBQkhZ28+nxd+sx8MBlsDtbl9WGX+r9Vq7b1vbjAYHCSC8/l83e12DxrA9Xr7LdFz3S6x7ZbyYDA49eVRgBBy1r58+VJozivLsq2jwH32/3U6nYNsHP/y5cveESy7V7JoANfrv+K67Wd9jtslxuPx1r/zxcXFqS+RAoSQs7RcLguPeN6aC9xn9ecmIId6898VwSJ7BfM8L7UQpsxK2fV6+yrRcxsZlZlTpf78\",\n" +
//                "  \"docusersn\": \"123456\",\n" +
//                "  \"enterdate\": \"2017/12/08\",\n" +
//                "  \"enterid\": \"123456\",\n" +
//                "  \"enternm\": \"123456\",\n" +
//                "  \"enterpgm\": \"123456\",\n" +
//                "  \"explan1\": \"Y\",\n" +
//                "  \"explan2\": \"Y\",\n" +
//                "  \"explan3\": \"Y\",\n" +
//                "  \"explan4\": \"Y\",\n" +
//                "  \"explan5\": \"Y\",\n" +
//                "  \"explan6\": \"Y\",\n" +
//                "  \"hosuseyn\": \"Y\",\n" +
//                "  \"item1\": \"Y\",\n" +
//                "  \"item2\": \"Y\",\n" +
//                "  \"item3\": \"Y\",\n" +
//                "  \"item4\": \"Y\",\n" +
//                "  \"lstplansn\": \"123456\",\n" +
//                "  \"lstplanstatcode\": \"01\",\n" +
//                "  \"orgmedno\": \"123456\",\n" +
//                "  \"orignfilegrpno\": \"123456\",\n" +
//                "  \"patiaddr1\": \"서울 관악구\",\n" +
//                "  \"patiaddr2\": \"서울대병원\",\n" +
//                "  \"patibday\": \"1974/12/20\",\n" +
//                "  \"patigender\": \"M\",\n" +
//                "  \"patiresid\": \"7412201111111\",\n" +
//                "  \"patisignimgfilegrpno\": \"\",\n" +
//                "  \"patistatecode\": \"02\",\n" +
//                "  \"patitn\": \"02-1234-2345\",\n" +
//                "  \"patitypfilegrpno\": \"123456\",\n" +
//                "  \"patiusernm\": \"123456\",\n" +
//                "  \"patividfilegrpno\": \"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAcIAAAGQCAYAAAA9XmC5AAAABHNCSVQICAgIfAhkiAAAFEhJREFUeJzt3d11GlnagNHTs+a+5AhQR4A6AuQIhCMARQCKQCgC4QhcisA4AtERCEcAEwFMBMxFf/hzqy2oKkBU8e69ltZc2BZluYeHU+enfluv1+sEAEH969QXAACnJIQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhMBOf/75Z3p4eEgfP35Mv/32W/rw4UO6vb1Nq9Xq1JcGe/ttvV6vT30RQL38+eefaTqd/vh6y/39fRqNRu92XXAMQgiklFL69u1bmkwmaTKZFB7pXVxcpOVyeeQrg+MSQgisSvxe8xZC0/371BcAvK/ZbJaenp5Snud7z/F1Op0DXRWcjhBCAKvVKn379i2Nx+M0m832/n5ZlqVut5vG4/EBrg5OSwjhjC0Wi/Tw8JDyPN/r+2RZlq6vr398XV1dHeYCoQaEEM7QarVKd3d3ewXw5uZG+AhBCOGMrFar9Pnz5zQejyvN//V6vXR9fZ263W66uLg4whVC/QghnIF9Anhzc5O63a74EZYQQsM9PT2l0WiUFotF4T/TarXScDhM3W43XV5eHu3aoAmEEBpqOp2m29vbUgHs9Xqp3++n6+vro10XNI0QQsPMZrN0d3e39eiz13q9XhqNRkZ/8AtCCA1RZSWoAMJuQggN8PDwUGohTKfTSePx2LYHKEAIocYmk0m6u7srPA/YbrfTeDw2BwglCCHU0GKxSLe3t4XnAVutVhqNRqnf7x/1uuAceTAv1MhmHvD3338vFMEsy9L9/X1aLBYiCBUZEUJNPD09peFwWHgesNfrpfF4bBM87EkI4cTKboewEAYOy61ROJHNbdA//vijUARbrVb6+vVrmk6nIggHZEQIJzCZTNLt7W2h26BZlqXhcJhGo9HxLwwCEkJ4R6vVKt3e3qbJZFLo95sHhONzaxTeydPTU/r9998LRbDdbqfn5+eU57kIwpEZEcKRldkTmGVZGo1GaTgcHv/CgJSSESEc1efPnwsvhhkMBmmxWIggvDMjQjiC2WyWbm9v02w22/l7W61WyvPcsWhwIkaEcGAPDw/pjz/+KBTB+/v7NJvNRBBOyIgQDmQ6naa7u7tCAWy32ynPc/sBoQaMCGFPm43xHz9+3BnBzdmgs9lMBKEmjAhhD9PpNN3e3hZ6TFKn00l5nh/8IbmLxSL95z//+bEg5/X/ppTS9fV16vf7qdfrHfS14Rz8tl6v16e+CGiaMhvjj7ElYjqdpqenpzSZTAof0p1SSo+Pj1alwitCCCWVOR7t5uYmjcfjg4wCF4tFenp6SnmeF35Q72tXV1fp5eVl72sparVapW/fvqXpdJpms1mazWbp8vIyXV1dpcfHx4OPjqEKIYSCFotFuru7KzwKzPM8dbvdvV5zE5LxeFxoEU4Rx/6/fNFrvri4SC8vL2LIyVksAwVsNsYXiWCv10uLxaJyBFerVXp6ekqfPn1KHz58SP1+/2AR7HQ6B/k+v7I5QafoNa9WqzQej492PVCUxTKwRZnj0fbZGL8ZRU0mk8IHcldxjPnBxWKRHh4eUp7npf/soQIP+xBCeMPDw0PhRx8NBoM0Go1KH5C9iUjZRS+vdTqddHl5mWazWfr+/fsvf8/Nzc3et2p/tk8ANxwoTi2sgb95eXlZX11drVNKO7/a7fb6+fm59GvM5/N1v98v9BrbXvvLly/r5XL547ovLi7e/P3z+fwgP59DXPvm68uXLwe5JtiHEML/WS6X69FoVPhN/P7+vvRrPD8/7xWRVqu1HgwG/4jacrncGsEq1/raIQO4+btAHQghrP8K1OXlZaE38E6ns355eSn9/a+vrysFI8uyda/Xe3PkuVwut45g2+32Xj+bqgHs9Xrrdrv95q9XGUnDMQghoS2Xy/VwOCwcpMfHx1LfP8/zwrdZfxW/r1+/7rz+bd8/y7LKt0T3CeB8Pl/P5/OtvwfqQggJq+wosGhQlsvlejweF/7erwOxK34/v86uyFYZdS2Xy70CuLFtBHyo+Uo4BCEkpKJzgVmWFV7QMZ/P18PhcOtc3bbRX5k4FIlg2YUomznSstf/q2v/+vWr0SCNIYSEUmZF6M3NzY8VmdtUXQCTZdn6/v6+0Gu8tuv1ysYmz/PSI9i34r1cLt/8XvvcqoVjEULCGI/Hhd7gW61WoduT+8z/VQ3gen3YCFZZxLNr9Lrt+g6xerWuXs+p9vv9U18SBQkhZ28+nxd+sx8MBlsDtbl9WGX+r9Vq7b1vbjAYHCSC8/l83e12DxrA9Xr7LdFz3S6x7ZbyYDA49eVRgBBy1r58+VJozivLsq2jwH32/3U6nYNsHP/y5cveESy7V7JoANfrv+K67Wd9jtslxuPx1r/zxcXFqS+RAoSQs7RcLguPeN6aC9xn9ecmIId6898VwSJ7BfM8L7UQpsxK2fV6+yrRcxsZlZlTpf78\",\n" +
//                "  \"patizipno\": \"123456\",\n" +
//                "  \"planday\": \"2017/12/08\",\n" +
//                "  \"readacccode\": \"01\",\n" +
//                "  \"readaccoth\": \"123456\",\n" +
//                "  \"regno\": \"123456\",\n" +
//                "  \"repsignimgfilegrpno\": \"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAcIAAAGQCAYAAAA9XmC5AAAABHNCSVQICAgIfAhkiAAAFEhJREFUeJzt3d11GlnagNHTs+a+5AhQR4A6AuQIhCMARQCKQCgC4QhcisA4AtERCEcAEwFMBMxFf/hzqy2oKkBU8e69ltZc2BZluYeHU+enfluv1+sEAEH969QXAACnJIQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhMBOf/75Z3p4eEgfP35Mv/32W/rw4UO6vb1Nq9Xq1JcGe/ttvV6vT30RQL38+eefaTqd/vh6y/39fRqNRu92XXAMQgiklFL69u1bmkwmaTKZFB7pXVxcpOVyeeQrg+MSQgisSvxe8xZC0/371BcAvK/ZbJaenp5Snud7z/F1Op0DXRWcjhBCAKvVKn379i2Nx+M0m832/n5ZlqVut5vG4/EBrg5OSwjhjC0Wi/Tw8JDyPN/r+2RZlq6vr398XV1dHeYCoQaEEM7QarVKd3d3ewXw5uZG+AhBCOGMrFar9Pnz5zQejyvN//V6vXR9fZ263W66uLg4whVC/QghnIF9Anhzc5O63a74EZYQQsM9PT2l0WiUFotF4T/TarXScDhM3W43XV5eHu3aoAmEEBpqOp2m29vbUgHs9Xqp3++n6+vro10XNI0QQsPMZrN0d3e39eiz13q9XhqNRkZ/8AtCCA1RZSWoAMJuQggN8PDwUGohTKfTSePx2LYHKEAIocYmk0m6u7srPA/YbrfTeDw2BwglCCHU0GKxSLe3t4XnAVutVhqNRqnf7x/1uuAceTAv1MhmHvD3338vFMEsy9L9/X1aLBYiCBUZEUJNPD09peFwWHgesNfrpfF4bBM87EkI4cTKboewEAYOy61ROJHNbdA//vijUARbrVb6+vVrmk6nIggHZEQIJzCZTNLt7W2h26BZlqXhcJhGo9HxLwwCEkJ4R6vVKt3e3qbJZFLo95sHhONzaxTeydPTU/r9998LRbDdbqfn5+eU57kIwpEZEcKRldkTmGVZGo1GaTgcHv/CgJSSESEc1efPnwsvhhkMBmmxWIggvDMjQjiC2WyWbm9v02w22/l7W61WyvPcsWhwIkaEcGAPDw/pjz/+KBTB+/v7NJvNRBBOyIgQDmQ6naa7u7tCAWy32ynPc/sBoQaMCGFPm43xHz9+3BnBzdmgs9lMBKEmjAhhD9PpNN3e3hZ6TFKn00l5nh/8IbmLxSL95z//+bEg5/X/ppTS9fV16vf7qdfrHfS14Rz8tl6v16e+CGiaMhvjj7ElYjqdpqenpzSZTAof0p1SSo+Pj1alwitCCCWVOR7t5uYmjcfjg4wCF4tFenp6SnmeF35Q72tXV1fp5eVl72sparVapW/fvqXpdJpms1mazWbp8vIyXV1dpcfHx4OPjqEKIYSCFotFuru7KzwKzPM8dbvdvV5zE5LxeFxoEU4Rx/6/fNFrvri4SC8vL2LIyVksAwVsNsYXiWCv10uLxaJyBFerVXp6ekqfPn1KHz58SP1+/2AR7HQ6B/k+v7I5QafoNa9WqzQej492PVCUxTKwRZnj0fbZGL8ZRU0mk8IHcldxjPnBxWKRHh4eUp7npf/soQIP+xBCeMPDw0PhRx8NBoM0Go1KH5C9iUjZRS+vdTqddHl5mWazWfr+/fsvf8/Nzc3et2p/tk8ANxwoTi2sgb95eXlZX11drVNKO7/a7fb6+fm59GvM5/N1v98v9BrbXvvLly/r5XL547ovLi7e/P3z+fwgP59DXPvm68uXLwe5JtiHEML/WS6X69FoVPhN/P7+vvRrPD8/7xWRVqu1HgwG/4jacrncGsEq1/raIQO4+btAHQghrP8K1OXlZaE38E6ns355eSn9/a+vrysFI8uyda/Xe3PkuVwut45g2+32Xj+bqgHs9Xrrdrv95q9XGUnDMQghoS2Xy/VwOCwcpMfHx1LfP8/zwrdZfxW/r1+/7rz+bd8/y7LKt0T3CeB8Pl/P5/OtvwfqQggJq+wosGhQlsvlejweF/7erwOxK34/v86uyFYZdS2Xy70CuLFtBHyo+Uo4BCEkpKJzgVmWFV7QMZ/P18PhcOtc3bbRX5k4FIlg2YUomznSstf/q2v/+vWr0SCNIYSEUmZF6M3NzY8VmdtUXQCTZdn6/v6+0Gu8tuv1ysYmz/PSI9i34r1cLt/8XvvcqoVjEULCGI/Hhd7gW61WoduT+8z/VQ3gen3YCFZZxLNr9Lrt+g6xerWuXs+p9vv9U18SBQkhZ28+nxd+sx8MBlsDtbl9WGX+r9Vq7b1vbjAYHCSC8/l83e12DxrA9Xr7LdFz3S6x7ZbyYDA49eVRgBBy1r58+VJozivLsq2jwH32/3U6nYNsHP/y5cveESy7V7JoANfrv+K67Wd9jtslxuPx1r/zxcXFqS+RAoSQs7RcLguPeN6aC9xn9ecmIId6898VwSJ7BfM8L7UQpsxK2fV6+yrRcxsZlZlTpf78\",\n" +
//                "  \"repusernm\": \"123456\",\n" +
//                "  \"selfyn\": \"Y\",\n" +
//                "  \"updatedate\": \"2017/12/08\",\n" +
//                "  \"updateid\": \"123456\",\n" +
//                "  \"updatenm\": \"123456\",\n" +
//                "  \"updatepgm\": \"123456\"\n" +
//                "}"

        val servlet = "connection.servlet=" + Define.ozEndPoint
        val reportname = "\n" + "connection.reportname=" + url
        val pcount = "\n" + "connection.pcount=2"
        val args1 = "\n" + "connection.args1=jsondata="+option
        val args2 = "\n" + "connection.args2=userInfo="+userInfo
        val errorcommand = "\n" + "viewer.errorcommand=true"
        val font = "\n" + "font.fontnames=font1,font2"
        val font1name = "\n" + "font.font1.name=돋움"
        val font1url = "\n" + "font.font1.url=res://dotum.ttc"
        val font2name = "\n" + "font.font2.name=돋움체"
        val font2url = "\n" + "font.font2.url=res://dotum.ttc"
        val zoombydoubletap = "\n" + "viewer.zoombydoubletap=false"
        val minzoom = "\n" + "viewer.minzoom=70"
        val usetoolbar = "\n" + "viewer.usetoolbar=true"
        val progresscommand = "\n" + "viewer.progresscommand=true"
        val signpad_type = "\n" + "eform.signpad_type=zoom"
//        var params = servlet + reportname + pcount + args1 + zoombydoubletap + minzoom + usetoolbar + progresscommand + signpad_type
        var params = servlet + reportname + pcount + args1 + args2 + zoombydoubletap + minzoom + errorcommand + usetoolbar + progresscommand + font + font1name + font1url + font2name + font2url

        //오즈 뷰어를 표시할 View 또는 ViewGroup 생성
        val frameLayout = FrameLayout(this)

        //오즈 뷰어 생성
        viewer = OZReportAPI.createViewer(frameLayout, oZReportCommandListener, params)
        e_form_viewer.addView(frameLayout)
//        viewer.Script("disable_input_all") // 모든 입력 컴포넌트 disable

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        Dlog.e("requestCode : $requestCode")
        Dlog.e("permissions : " + permissions[0])
        Dlog.e("grantResults : " + grantResults[0])

        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            initOZViewer()
        } else {
            finish()
            Toast.makeText(this@EFormViewerActivity, "'허용' 해주셔야 이용 가능합니다.", Toast.LENGTH_LONG).show()
        }
    }

}