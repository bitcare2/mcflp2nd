package com.bit.bitcare.mcflp2nd.ui.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
import android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.webkit.*
import android.widget.ZoomButtonsController
import com.bit.bitcare.mcflp2nd.BaseApplication
import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.util.Dlog
import kotlinx.android.synthetic.main.fragment_user_search.*
import com.bit.bitcare.mcflp2nd.ui.activity.MainActivity
import android.view.KeyEvent.KEYCODE_BACK
import android.view.ViewTreeObserver
import android.widget.Toast
import com.bit.bitcare.mcflp2nd.ui.activity.LoginActivity
import com.bit.bitcare.mcflp2nd.ui.activity.PopUpActivity
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class UserSearchFragment() : BaseFragment() {
    //    private var fileUri:Uri? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_user_search, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        settingWebView(webview_user_search)
        Dlog.e("Define.serverIP: " + Define.serverIP)
        Dlog.e("Define.userSearchUrl : "+Define.userSearchUrl+ PreferenceUtil(activity).getStringExtra("token"))
        webview_user_search!!.loadUrl(Define.userSearchUrl+PreferenceUtil(activity).getStringExtra("token"))
    }

    override fun getCurrentViewName():String{
        return this::class.java.simpleName
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == AppCompatActivity.RESULT_OK) {
            Dlog.e("resultCode = $resultCode")
            Dlog.e("requestCode = $requestCode")
            Dlog.e("data = ${data.toString()}")

            when (requestCode) {
                Define.REQUEST_REG_FORM -> {
                    Dlog.e("data = " + data!!.getStringExtra("type"))
                    Dlog.e("data = " + data.getStringExtra("idtype"))
                    Dlog.e("data = " + data.getStringExtra("datakey"))
                    Dlog.e("data = " + data.getStringExtra("usernm"))

                    webView!!.loadUrl("javascript:" + data!!.getStringExtra("type") + ".regFormCallback('" + data.getStringExtra("idtype") + "','"
                            + data.getStringExtra("datakey") + "','"
                            + data.getStringExtra("usernm") + "')")

                    //임시저장 목록 갱신
                    val TemporarySaveFragmentTag = PreferenceUtil(context).getStringExtra("TemporarySaveFragmentTag")
                    val temporarySaveFragment = activity!!.supportFragmentManager.findFragmentByTag(TemporarySaveFragmentTag) as TemporarySaveFragment
                    temporarySaveFragment?.webView!!.reload()

                    //관리자가 아닐때 업무이력도 갱신
//                    if(!"Y".equals(PreferenceUtil(context).getStringExtra("isAdmin"))){
                    val JobHistoryFragmentTag = PreferenceUtil(context).getStringExtra("JobHistoryFragmentTag")
                    val jobHistoryFragment = activity!!.supportFragmentManager.findFragmentByTag(JobHistoryFragmentTag) as JobHistoryFragment
                    jobHistoryFragment?.webView!!.reload()
//                    }
                }
            }
        }
    }



}

