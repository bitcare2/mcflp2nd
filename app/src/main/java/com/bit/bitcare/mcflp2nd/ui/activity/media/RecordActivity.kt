package com.bit.bitcare.mcflp2nd.ui.activity.media

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.SurfaceTexture
import android.graphics.drawable.Drawable
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraCaptureSession
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraDevice
import android.hardware.camera2.CameraManager
import android.hardware.camera2.CaptureRequest
import android.hardware.camera2.params.StreamConfigurationMap
import android.media.CamcorderProfile
import android.media.MediaRecorder
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.util.Log
import android.util.Size
import android.view.Surface
import android.view.TextureView
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.R.id.record_button
import com.bit.bitcare.mcflp2nd.ui.activity.BaseActivity
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.Dlog
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil
import com.google.firebase.FirebaseException
import kotlinx.android.synthetic.main.activity_record.*

import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

@RequiresApi
class RecordActivity : BaseActivity() {

    private var previewSize: Size? = null
    private var cameraDevice: CameraDevice? = null
    private var previewBuilder: CaptureRequest.Builder? = null
    private var previewSession: CameraCaptureSession? = null

    private var textureView: TextureView? = null
    private var leftTimeView: TextView? = null
    private var recordButton: Button? = null

    private var mediaRecorder: MediaRecorder? = null
    private var leftTime: Int = 0
    private var timer: Timer? = null
    private var timerTask: TimerTask? = null

    private var fileName: String? = null
    private var willSendFile:File? = null

    private var isRecording: Boolean = false
//        get() = mediaRecorder != null

    val outputMediaFile: File?
        get() {
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED, ignoreCase = true)) {
                val filedir = File(Environment.getExternalStorageDirectory(), "NIBP")
                if (!filedir.exists()) {
                    filedir.mkdir()
                }
                val currenttime = SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA).format(Date())
                return File(filedir.absolutePath + File.separator + "Video_"+ currenttime + ".mp4")
            }
            return null
        }

    private val surfaceTextureListener = object : TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                openCamera()
            }else {
                Toast.makeText(this@RecordActivity, "롤리팝 미만 버전에서는 해당 기능을 제공하지 않습니다.", Toast.LENGTH_LONG).show()
            }

        }

        override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture, width: Int, height: Int) {

        }

        override fun onSurfaceTextureDestroyed(surface: SurfaceTexture): Boolean {
            return false
        }

        override fun onSurfaceTextureUpdated(surface: SurfaceTexture) {

        }
    }

    private val deviceStateCallback = @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    object : CameraDevice.StateCallback() {

            override fun onOpened(camera: CameraDevice) {
                cameraDevice = camera
                showPreview()
            }

            override fun onClosed(camera: CameraDevice) {
                super.onClosed(camera)
                stopRecording(false)
            }

            override fun onDisconnected(camera: CameraDevice) {

            }

            override fun onError(camera: CameraDevice, error: Int) {

            }
    }

    private val captureStateCallback = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        object : CameraCaptureSession.StateCallback() {

            override fun onConfigured(session: CameraCaptureSession) {
                previewSession = session
                updatePreview()
            }

            override fun onConfigureFailed(session: CameraCaptureSession) {

            }
        }
    } else {
        Toast.makeText(this@RecordActivity, "롤리팝 미만 버전에서는 해당 기능을 제공하지 않습니다.", Toast.LENGTH_LONG).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record)

        textureView = findViewById<View>(R.id.preview) as TextureView
        leftTimeView = findViewById<View>(R.id.left_time) as TextView
        recordButton = findViewById<View>(R.id.record_button) as Button
        recordButton?.setOnClickListener {
            if (isRecording) {
                stopRecording(true)
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    startRecording()
                }else{
                    Toast.makeText(this@RecordActivity, "롤리팝 미만 버전에서는 해당 기능을 제공하지 않습니다.", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        startPreview()
    }

    override fun onPause() {
        super.onPause()
        stopPreview()
        stopRecording(false)
    }

    private fun startPreview() {
        if (textureView!!.isAvailable) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                openCamera()
            }else{
                Toast.makeText(this@RecordActivity, "롤리팝 미만 버전에서는 해당 기능을 제공하지 않습니다.", Toast.LENGTH_LONG).show()
            }

        } else {
            textureView!!.surfaceTextureListener = surfaceTextureListener
        }
    }

    private fun stopPreview() {
        if (previewSession != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                previewSession!!.close()
            }
            previewSession = null
        }
        if (cameraDevice != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cameraDevice!!.close()
            }
            cameraDevice = null
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun openCamera() {
        val manager = getSystemService(Context.CAMERA_SERVICE) as CameraManager

        try {
            var backCameraId: String? = null

            for (cameraId in manager.cameraIdList) {
                val characteristics = manager.getCameraCharacteristics(cameraId)

                val cameraOrientation = characteristics.get(CameraCharacteristics.LENS_FACING)!!

                if (cameraOrientation == CameraCharacteristics.LENS_FACING_BACK) {
                    backCameraId = cameraId
                    break
                }
            }

            if (backCameraId == null) {
                return
            }

            val cameraCharacteristics = manager.getCameraCharacteristics(backCameraId)

            val streamConfigurationMap = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)

            previewSize = streamConfigurationMap!!.getOutputSizes(SurfaceTexture::class.java)[0]

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                return
            }

            manager.openCamera(backCameraId, deviceStateCallback, null)

            stopPreview()

        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun showPreview() {
        val surfaceTexture = textureView!!.surfaceTexture
        val surface = Surface(surfaceTexture)

        try {
            previewBuilder = cameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
            previewBuilder!!.addTarget(surface)

            cameraDevice!!.createCaptureSession(Arrays.asList(surface), captureStateCallback as CameraCaptureSession.StateCallback, null)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun updatePreview() {
        try {
            previewSession!!.setRepeatingRequest(previewBuilder!!.build(), null, null)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun startRecording() {
        isRecording = true

        recordButton!!.setBackgroundResource(R.drawable.ic_stop_gray_48dp)

        if (mediaRecorder == null) {
            mediaRecorder = MediaRecorder()
        }

        fileName = outputMediaFile!!.absolutePath
        mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
        mediaRecorder!!.setVideoSource(MediaRecorder.VideoSource.SURFACE)
        //        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        //        mediaRecorder.setVideoFrameRate(24);
        //        mediaRecorder.setVideoSize(previewSize.getWidth(), previewSize.getHeight());
        //        mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        //        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);


        // 현재 앱이 실행되는 카메라가 지원하는
        // 최대 화질의 설정 프로필을 구한다.
        // 안드로이드 기기에 탑재되는 카메라에 따라
        // 결정되는 녹화 사이즈 등은 모두 달라진다.
        val camcorderProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_480P)

        // 일부 기종에서 최대 화질 프로필 과
        // 카메라 하드웨어 가 지원하는 최대 사이즈가 다른 문제가 발생한다.
        // 실제 카메라 하드웨어로 부터 구해진 previewSize 와
        // 최대 화질 프로필 API 를 통해 구해진 최대 지원 사이즈를 비교해서
        // 더 작은 쪽으로 설정을 바꾼다.
        if (camcorderProfile.videoFrameWidth > previewSize!!.width || camcorderProfile.videoFrameHeight > previewSize!!.height) {
            camcorderProfile.videoFrameWidth = previewSize!!.width
            camcorderProfile.videoFrameHeight = previewSize!!.height
        }

        mediaRecorder!!.setProfile(camcorderProfile)
        //        mediaRecorder.setVideoEncodingBitRate(5000000);
        mediaRecorder!!.setOutputFile(fileName)
        mediaRecorder!!.setOrientationHint(90)

        try {
            mediaRecorder!!.prepare()
        } catch (e: IOException) {
            e.printStackTrace()
            return
        }

        val surfaces = ArrayList<Surface>()

        val surfaceTexture = textureView!!.surfaceTexture
        val previewSurface = Surface(surfaceTexture)
        surfaces.add(previewSurface)

        val mediaRecorderSurface = mediaRecorder!!.surface
        surfaces.add(mediaRecorderSurface)

        try {
            previewBuilder = cameraDevice?.createCaptureRequest(CameraDevice.TEMPLATE_RECORD)

            previewBuilder?.addTarget(previewSurface)
            previewBuilder?.addTarget(mediaRecorderSurface)

            cameraDevice?.createCaptureSession(surfaces, captureStateCallback as CameraCaptureSession.StateCallback, null)

            if(mediaRecorder != null){
                mediaRecorder?.start()

                timerTask = object : TimerTask() {
                    override fun run() {
                        updateNumber()
                    }
                }

                leftTime = Define.VIDEO_RECORD_TIME

                timer = Timer()
                timer?.schedule(timerTask, 0, 1000)
            }else{
                Toast.makeText(this@RecordActivity,"다시 녹화 버튼을 눌러주세요",Toast.LENGTH_LONG).show()
            }


            Handler().postDelayed({ stopRecording(true) }, Define.VIDEO_RECORD_MILLI_TIME)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    private fun updateNumber() {
        Log.e("Tag", leftTime.toString() + "")
        if (leftTime == 0) {
            timer!!.cancel()
            timer = null
        } else {
            runOnUiThread {
                leftTimeView!!.text = leftTime.toString()
                leftTime--
            }
        }
    }

    private fun stopRecording(showPreview: Boolean) {
        isRecording = false

        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
        leftTimeView!!.text = ""

        stopPreview()

        if (mediaRecorder != null) {
            mediaRecorder!!.stop()
            mediaRecorder!!.reset()
            mediaRecorder!!.release()
            mediaRecorder = null
        }

        var resultIntent = Intent()
        resultIntent.putExtra("result",fileName)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()

    }

}