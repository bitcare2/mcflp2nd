package com.bit.bitcare.mcflp2nd.ui.activity

import android.content.Intent
import android.content.Intent.*
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.StringRequestListener
import com.bit.bitcare.mcflp2nd.R
import com.bit.bitcare.mcflp2nd.data.model.AppSetupInfoVo
import com.bit.bitcare.mcflp2nd.data.model.MenuVo
import com.bit.bitcare.mcflp2nd.util.Define
import com.bit.bitcare.mcflp2nd.util.Dlog
import com.bit.bitcare.mcflp2nd.util.PreferenceUtil
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_ip_change_test.*
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : BaseActivity() {
    lateinit var pInfo: PackageInfo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
/////////////////////////////////////////////////////////////////
        var serverIp: String = PreferenceUtil(this@SplashActivity).getStringExtra("serverIp")

        if(serverIp.equals("")){
        }else{
            Define.serverIP = serverIp
            Define.userSearchUrl               = "$serverIp/mobile/reg/mng/orgWrtMng/index.do?tabFlag=A&token="
            Define.temporarySaveListUrl        = "$serverIp/mobile/reg/mng/orgWrtMng/index.do?tabFlag=B&token="
            Define.JobHistoryUrl               = "$serverIp/mobile/reg/mng/orgWrtMng/index.do?tabFlag=C&token="
        }
//        Toast.makeText(this@SplashActivity, "serverip : "+Define.serverIP, Toast.LENGTH_LONG).show()
/////////////////////////////////////////////////////나중에 여기 위에 지워야함

        getAppSetupInfo()
//        Handler().postDelayed({
//            val intent = Intent(this, LoginActivity::class.java)
//            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
//            startActivity(intent)
//            overridePendingTransition(0,0)
//            finish()
//        }, 1000)
    }

    private fun getAppSetupInfo(){
        Dlog.e("getAppSetupInfo")
        AndroidNetworking.post(Define.appSetupInfoServerIP+"/temp/setup")
                .addQueryParameter("appClsCode","02")
                .addQueryParameter("newYn","Y")
                .addQueryParameter("appOperSysMfg","1")
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(stringRequestListener)
    }

    val stringRequestListener = object: StringRequestListener {
        override fun onResponse(response:String) {
            Dlog.e("stringRequestListener : " + response)

            var appSetupInfoVo: AppSetupInfoVo = gson.fromJson(response, AppSetupInfoVo::class.java)
            Dlog.e("appSetupInfoVo : " + appSetupInfoVo.toString())

            var serverIp = ""
            var mobileServerIp = ""

            when(appSetupInfoVo.appOpfg){
                "1" -> { //운영서버
                    serverIp = "https://intra.lst.go.kr"
                    mobileServerIp = "https://mobile.lst.go.kr"
                    PreferenceUtil(this@SplashActivity).putStringExtra("serverIP", "https://intra.lst.go.kr")
                    PreferenceUtil(this@SplashActivity).putStringExtra("mobileServerIP", "https://mobile.lst.go.kr")
                    Dlog.e("https://intra.lst.go.kr 세팅됨")
                    Dlog.e("https://mobile.lst.go.kr 세팅됨")
                }
                "2" -> { //G2E테스트서버
                    serverIp = "https://nibp-intra.g2e.co.kr"
                    mobileServerIp = "http://nibp-mobile.g2e.co.kr"
                    PreferenceUtil(this@SplashActivity).putStringExtra("serverIP", "https://nibp-intra.g2e.co.kr")
                    PreferenceUtil(this@SplashActivity).putStringExtra("mobileServerIP", "http://nibp-mobile.g2e.co.kr")
                    Dlog.e("https://nibp-intra.g2e.co.kr 세팅됨")
                    Dlog.e("http://nibp-mobile.g2e.co.kr 세팅됨")
                }
            }

//            serverIp = "https://nibp-intra.g2e.co.kr"
//            mobileServerIp = "http://nibp-mobile.g2e.co.kr"
//            PreferenceUtil(this@SplashActivity).putStringExtra("serverIP", "https://nibp-intra.g2e.co.kr")
//            PreferenceUtil(this@SplashActivity).putStringExtra("mobileServerIP", "http://nibp-mobile.g2e.co.kr")

            Define.serverIP = serverIp
            Define.userSearchUrl                = "$serverIp/mobile/reg/mng/orgWrtMng/index.do?tabFlag=A&token="
            Define.temporarySaveListUrl         = "$serverIp/mobile/reg/mng/orgWrtMng/index.do?tabFlag=B&token="
            Define.JobHistoryUrl                = "$serverIp/mobile/reg/mng/orgWrtMng/index.do?tabFlag=C&token="
            Define.certLoginUrl                 = "$serverIp/rm/mobile/mobilecert.do"

            Define.mobileServerIP               = mobileServerIp


            //// for Test
//            appSetupInfoVo.appMummVer = "0.09"
//            appSetupInfoVo.appOpMsg = "공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지"

            when(appSetupInfoVo.appMummVer){
                "" -> {
                    checkNotice(appSetupInfoVo.appOpMsg!!)
                }
                else -> {
                    var versionName: String = ""

                    pInfo = packageManager.getPackageInfo(packageName, PackageManager.GET_META_DATA)

                    versionName = pInfo!!.versionName
                    Dlog.e("versionName.toLong() : " + versionName.toFloat())
                    Dlog.e("appSetupInfoVo.appMummVer!!.toLong() : " + appSetupInfoVo.appMummVer!!.toFloat() )

                    if(appSetupInfoVo.appMummVer!!.toFloat() > versionName.toFloat()){
                        MaterialDialog(this@SplashActivity)
                                .title(R.string.update_check)
                                .message(R.string.update_check_message)
                                .positiveButton (R.string.move){
                                    var goMarket: Intent = Intent(Intent.ACTION_VIEW)
                                    goMarket.setData(Uri.parse("market://details?id=com.bit.bitcare.mcflp2nd"))
                                    startActivity(goMarket)
                                }
                                .negativeButton(R.string.close){

                                }
                                .show()
                    }else{
                        checkNotice(appSetupInfoVo.appOpMsg!!)
                    }
                }
            }
        }
        override fun onError(error: ANError) {
            if (error.errorCode !== 0) {
                // received error from server
                // error.getErrorCode() - the error code from server
                // error.getErrorBody() - the error body from server
                // error.getErrorDetail() - just an error detail
                Dlog.d("onError errorCode : " + error.errorCode)
                Dlog.d("onError errorBody : " + error.errorBody)
                Dlog.d("onError errorDetail : " + error.errorDetail)
            } else {
                // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                Dlog.d("onError errorDetail : " + error.errorDetail)
            }
        }
    }

    private fun checkNotice(noticeString: String){
        when(noticeString){
            "" -> {
                goToLogin()
            }
            else -> {
                MaterialDialog(this@SplashActivity)
                        .title(R.string.notice)
                        .positiveButton (R.string.done){
                            goToLogin()
                        }
                        .show{
                            message(text = noticeString)
                        }
            }
        }
    }

    private fun goToLogin(){
        val intent = Intent(this@SplashActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }
}