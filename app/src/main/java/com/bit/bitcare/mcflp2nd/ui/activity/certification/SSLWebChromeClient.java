package com.bit.bitcare.mcflp2nd.ui.activity.certification;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

/**
 * Created by pol808 on 2016-08-26.
 */
public class SSLWebChromeClient extends WebChromeClient {

    public final String AlertTitle = "info";

    Context context;

    public SSLWebChromeClient(Context context) {
        this.context = context;
    }

    @Override
    public boolean onJsAlert(WebView view, String url, String message, final android.webkit.JsResult result)
    {
        new AlertDialog.Builder(this.context)
                .setTitle(AlertTitle)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok,
                        new AlertDialog.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                result.confirm();
                            }
                        })
                .setCancelable(false)
                .create()
                .show();

        return true;
    };
}