package com.bit.bitcare.mcflp2nd.util

import oz.util.barcode.common.StringUtils
import java.text.SimpleDateFormat
import java.util.*

open class Util {
     companion object {
         fun getToDate():String{
             var today:Date = Date()
             var simpleDateFormat:SimpleDateFormat = SimpleDateFormat("yyyyMMdd")
             return simpleDateFormat.format(today)
         }

        fun getFileSize(size:String):String{
            val gubn = arrayOf("Byte", "KB", "MB")
            var returnSize = String()
            var gubnKey = 0
            var changeSize = 0.0
            var fileSize: Long = 0
            try {
                fileSize = java.lang.Long.parseLong(size)
                var x = 0
                while (fileSize / 1024.toDouble() > 0) {
                    gubnKey = x
                    changeSize = fileSize.toDouble()
                    x++
                    fileSize /= 1024.toDouble().toLong()
                }
                returnSize = changeSize.toString() + gubn[gubnKey]
            } catch (ex: Exception) {
                returnSize = "0.0 Byte"
            }

            println("getFileSize:$returnSize")
            return returnSize

        }

     }

    private var basePath = "/lst_nas"
    private var pathTmp = "/tmp"

    fun getTmpFilepath(filegrpno: Long?): String {
        val path = StringBuilder()
        Dlog.e("basePath : {$basePath} ")
        Dlog.e("pathTmp : {$pathTmp}")
        path.append(basePath)
        path.append(pathTmp)
        path.append(getTmpDay())
        path.append("/")
        path.append(filegrpno)
        path.append("/")
        return path.toString()
    }

    private fun getTmpDay(): String{
        val sdf = SimpleDateFormat("yyyyMMdd")
        return sdf.format(Date())
    }

}
