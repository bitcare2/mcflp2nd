package com.bit.bitcare.mcflp2nd.ui.activity.certification;

import android.content.Context;
import android.content.Intent;
import android.widget.EditText;

import com.nprotect.keycryptm.Defines;
import com.nprotect.keycryptm.IxConfigureInputItem;
import com.nprotect.keycryptm.IxKeypadManageHelper;

/**
 * Created by pol808 on 2017-03-20.
 */
public class NOSKeypad {

    private Context ctx;

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // KeyCrypt M
    IxKeypadManageHelper keypadMngHelper;
    private final static int REQUEST_CODE_KEYPAD = 0x8080;
    private final static int CRYPTO_PLUGIN_CODE = 0x800;

    private IxConfigureInputItem[] inputConfigs;
    /////////////////////////////////////////////////////////////////////////////////////////////////////


    public NOSKeypad(Context c) {
        this.ctx = c;
    }

    public void init(EditText editText) {
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // KeyCrypt M
        //
        /** 보안 키패드 헬퍼 객체 생성 **/

        keypadMngHelper = new IxKeypadManageHelper(this.ctx, REQUEST_CODE_KEYPAD); // 키패드 핼퍼 객체 생성
        keypadMngHelper.setUiVisibility(Defines.FLAG_UI_NO_INDICATOR); // 키패드 입력 시 화면 설정
        /** **/
        keypadMngHelper.setCryptoPlugin(CRYPTO_PLUGIN_CODE);

        /** 키패드 콜백 리스너 등록 **/
        keypadMngHelper.setSecureKeypadEventListener(new IxKeypadManageHelper.SecureKeypadEventListener() {
            /** 키패드 높이 변화 시 콜백 */
            @Override
            public void onKeypadChangeHeight(int height) {
            }

            /** 입력 제한 정책 위반 콜백 */
            @Override
            public void onInputViolationOccured(int errorCode) {
            }

            /** 포커싱 전환 시 콜백 */
            @Override
            public void onChangeEditText(EditText edittext) {
            }
        });

        inputConfigs = new IxConfigureInputItem[1];

        IxConfigureInputItem inputConfig = new IxConfigureInputItem(editText, Defines.KEYPAD_TYPE_QWERTY, Defines.SHUFFLE_TYPE_GAPKEY);
        inputConfig.setMinLength(5);
        inputConfig.setMaxLength(50);
        inputConfigs[0] = inputConfig;

        keypadMngHelper.configureInputBoxs(inputConfigs);
        /////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    /**
     * 입력 완료 후 암호 데이터
     */
    public String getResult(EditText id)
    {
        String ret = null;
//        /////////////////////////////////////////////////////////////////////////////////////////////////////
//        // KeyCrypt M
//        /** 입력 데이터 처리. 주의) 최종 처리 시에만 사용 **/
//        if ( !keypadMngHelper.postProcInputs() ) {
//            // TODO 오류 처리
//            return ret;
//        }
//        /////////////////////////////////////////////////////////////////////////////////////////////////////
//
//        IxResultItem passwdResult = keypadMngHelper.getInputResult(id);
//        if ( null == passwdResult) {
//            return ret;
//        }
//
//        byte[] masterKey = passwdResult.getMasterKey();
//        String encInputData = passwdResult.getEncData();

        ret = keypadMngHelper.getRealText(id);

        return ret;
    }

    public boolean finalize(int requestCode, int resultCode, Intent data)
    {

//        btxOK.setEnabled(true);
		/*-----------------------------------------------------------------------------*
	     * 키보드보안 결과 처리.
		 *-----------------------------------------------------------------------------*/
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // KeyCrypt M
        return keypadMngHelper.processResults(requestCode, resultCode, data);
        /////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    public void finish() {
        keypadMngHelper.finish();
    }
}
