package com.bit.bitcare.mcflp2nd.ui.activity.certification;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.webkit.JavascriptInterface;

import com.yettiesoft.koreamint.certificate.ManageCertificate;

/**
 * Created by pol808 on 2016-12-16.
 */
public class JSInterfaceNoSloth {

    private AppCompatActivity context;

    public JSInterfaceNoSloth(AppCompatActivity activity) {
        this.context = activity;
    }

    @JavascriptInterface
    public void setSloth(String url) {
        //////////////////////////////////////////////////////////////
        // 인증서 내보내기 시작점
        // web page를 통해 다음 함수가 호출된다.
        final String inputURL = url;

        //////////////////////////////////////////////////////////////
        // 인증서 관련 Manager 클래스 생성자 호출
        final ManageCertificate manager = new ManageCertificate(this.context);

        //////////////////////////////////////////////////////////////
        // 인증서 리스트 Class 호출
        // intent로 해당 url을 전달
        Intent intent = new Intent(context, KoreaMintMain.class);
        intent.putExtra("url", url);
        context.startActivityForResult(intent, 0);
    }
}
